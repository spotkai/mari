var Main = (function(window, $, Backbone, _) {
    "use strict";

    var main = {};      //instance

    //
    var MainView = Backbone.View.extend({
        el: $('body'),
        events: {
            'click #reloadPage': 'reloadPage'
        },

        initialize: function() {
            var _this = this;
            init();

            ///mediator facade for components
            vent.off();
            vent.stopListening();

            vent.on('ajaxRequest:start', function() {
                Util.startLoadingAnimation({top: 5, left: 350});
            });
            vent.on('ajaxRequest:done', function() {
                Util.stopLoadingAnimation();
            });

            vent.on('userData:loaded', function() {
                _this.router.navigate("#!projects", {trigger: true});
            });
            vent.on('userData:updated', function() {
                if(Backbone.history.fragment === '!projects') {
                    Projects.renderProjectList();
                    Projects.renderCalendar();
                }
            });

            ///

            window.location.hash = '';
            this.router = new MainRouter();
            Backbone.history.start();
            noticeView = new NoticeView();

            this.render();
        },

        //render components
        render: function() {
            this.$el.append(Dialog.loadComponent());
            this.$('#mainHeader').append($(UserPanel.loadComponent()));
        },

        //
        reloadPage: function() {
            var currentPage = Backbone.history.fragment.slice(1).capitalize();

            if(window[currentPage]) {
                Util.abortXhrPool();
                Dialog.destroyDialog();
                Main.hideNotice();
                window[currentPage].reloadPage();
            }
        },

        //mark current menu
        highlightHref: function() {
            var href = Backbone.history.location.hash,
                $appMenu = this.$('#appMenu');
            $appMenu.find('li').removeClass('selectedMenu');
            $appMenu.find('a[href$="' + href + '"]').closest('li').addClass('selectedMenu');
        }
    });


    //
    var MainRouter = Backbone.Router.extend({
        routes: {
            "!:pageName": "pageRoute"
        },

        pageRoute: function(pageName) {
            if(templates[pageName]) {
                var componentName = pageName.capitalize();
                if(this.lastComponent) {
                    this.unloadComponent(this.lastComponent);
                }
                this.lastComponent = componentName;
                mainView.highlightHref();
                $('#appContainer').html(templates[pageName]());
                if(window[componentName]) {
                    window[componentName].loadComponent();
                }
            }
            else {
                window.location.replace('error/404#');
            }
        },

        unloadComponent: function(component) {
            var bbComponent = window[component];
            for(var elem in bbComponent) {
                //close sub components
                if(bbComponent.hasOwnProperty(elem) && bbComponent[elem] instanceof Backbone.View) {
                    if(bbComponent[elem].collection) {
                        bbComponent[elem].collection.each(function(item) {
                            item.trigger('closeView');
                        });
                        bbComponent[elem].collection.off();
                        bbComponent[elem].collection.set([]);
                        delete bbComponent[elem].collection;
                    }
                    bbComponent[elem].closeView();
                    delete bbComponent[elem];
                }
            }
            //close component
            bbComponent.unloadComponent();
            delete bbComponent['props'];
            delete bbComponent[component.toLowerCase() + 'Model'];
        }

    });


    //
    var noticeView = null;
    var NoticeView = Backbone.View.extend({
        el: $('#notice'),
        events: {
            'click #noticeCloser': 'hideNotice',
            'click #noticeContent': function(ev) {
                if(typeof this.execCB === 'function') {
                    this.execCB(ev);
                }
            }
        },

        initialize: function() {
            this.TOP_POSITION = 13;
            this.BOTTOM_GAP = 50;
            this.$el.css({top: -this.$el.outerHeight(true)});
            this.$('#noticeContent').css({
                'max-height': $(window).height() - this.TOP_POSITION - this.$('#noticeTitle').outerHeight(true) - this.BOTTOM_GAP
            });
        },

        //
        showNotice: function(params) {
            var _this = this;

            $('#noticeShadow').addClass('fog');
            this.$('#noticeTitle').html(params.title);
            this.$('#noticeContent').html(params.msg);
            this.noticeType = params.type || 'ERROR';
            this.beforeClose = params.beforeClose || null;
            this.execCB = params.cb;

            this.prevWidth = this.$el.css('width');
            this.$el.css('width', params.width);
            this.$el.addClass('scene ' + this.noticeType)
                .animate({top: this.TOP_POSITION}, 400);

            if(this.noticeType === 'INFORM' && !params.keep) {
                window.setTimeout(function() {
                    _this.hideNotice();
                }, 2 * 1000);
            }
        },

        //
        hideNotice: function(force) {
            if(!this.$el.hasClass('scene')) {   //not active
                return false;
            }
            if(typeof this.beforeClose === 'function') {
                this.beforeClose();
                this.beforeClose = null;
            }

            var _this = this,
                duration = 400;
            if(force) {
                duration = 0;
            }
            this.$el.animate({top: -this.$el.outerHeight(true)}, duration,
                function() {
                    _this.$el.removeClass('scene ' + _this.noticeType);
                    _this.$el.css('width', _this.prevWidth);
                    _this.$('#noticeTitle').empty();
                    _this.$('#noticeContent').empty();
                    _this.execCB = null;
                });
            $('#noticeShadow').removeClass('fog');
        }
    });

    //
    function init() {
        //common ajaxError handler
        $.ajaxSetup({
            error: function(xhr, textStatus, errorThrown) {
                if(textStatus === 'timeout') {
                    Main.showNotice({msg: 'Время ожидания истекло.'});
                }
                else if(xhr.responseJSON) {
                    if(xhr.responseJSON.status === 'ERROR_REDIRECT') {
                        window.location.replace("error/401#");
                    }
                    else {
                        Main.showNotice({msg: xhr.responseJSON.data});
                    }
                }
                else {
                    Main.showNotice({msg: 'Ошибка при запросе данных:<br/>' + errorThrown + '.'});
                }
            }
        });

        //pickadate settings
        $.extend($.fn.pickadate.defaults, {
            monthsFull: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
            monthsShort: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            weekdaysFull: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            weekdaysShort: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
            today: false,
            clear: false,
            close: false,
            firstDay: 1,
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd'
        });

        //
        Backbone.View.prototype.closeView = function() {
            this.remove();    //delete from DOM, off & stopListening
            if(this.model) {
                this.model.off();
                delete this.model;
            }
        };

        //
        $(window).on('unload', function() {
            Util.abortXhrPool();
            $.ajaxSetup({async: false});
            Dialog.destroyDialog();  //for calling freeProject()
        });

        //
        window.onerror = function(message, source, line, column, error) {
            Main.showNotice({msg: message + '<br/>src: ' + source});
            Monitor.logJsError(message, source, error);
        };
    }


    ///layout API

    main.showNotice = function(params) {
        noticeView.showNotice(params);
    };

    main.hideNotice = function(force) {
        noticeView.hideNotice(force);
    };

    //
    main.syncError = function(model, xhr, options) {
        Dialog.destroyDialog();
        if(xhr.statusText === 'abort') {     //reloadPage() after hxr.abort()
            return false;
        }
        else if(xhr.statusText === 'timeout') {
            Main.showNotice({msg: 'Время ожидания истекло.'});
        }
        else if(xhr.responseJSON) {
            if(xhr.responseJSON.status === 'ERROR_REDIRECT') {
                window.location.replace("error/401#");
            }
            else {
                Main.showNotice({msg: xhr.responseJSON.data});
            }
        }
        else {
            Main.showNotice({msg: 'Ошибка при запросе данных.'});
        }
    };

    var mainView = new MainView();

    return main;

})(window, jQuery, Backbone, _);