var Login = (function(window, $, Backbone, _) {
    "use strict";

    var login = {},
        enterKey = 13,
        tabKey = 9;

    //
    login.LayoutLogin = Backbone.View.extend({
        el: $('#loginWrap'),
        events: {
            //'blur #loginName': 'checkUserName',
            'click button[name=enterLogin]': 'loginUser',
            'keyup #formLogin input': 'checkKeyup',
            'focus #formLogin input': function() {
                this.$('#loginMsg').empty();
            },
            'focus #formReg input': function() {
                this.$('#regMsg').empty();
            },
            'click #openReg': 'openRegistration',
            'change #userRegImg': 'loadUserRegImg',
            'click button[name=enterReg]': 'registerUser'
        },

        initialize: function() {
            ///mediator facade for components
            vent.off();
            vent.stopListening();

            vent.on('ajaxRequest:start', function() {
                Util.startLoadingAnimation({top: 5, right: 5});
            });
            vent.on('ajaxRequest:done', function() {
                Util.stopLoadingAnimation();
            });
            ///

            this.render();
            this.$('input:first').focus();
        },

        //render components
        render: function() {
        },

        //
        checkUserName: function(event) {
            var loginName = this.$('#loginName').val(),
                relatedTarget = event.relatedTarget || event.fromElement;
            if(relatedTarget && relatedTarget.getAttribute('name') === 'enterLogin') {
                this.loginUser();
            }
            else if(loginName.length) {
                var _this = this,
                    formData = {loginName: loginName};
                Util.requestJSON('login/checkUserName', formData)
                    .done(function(msg) {
                        if(msg.status === 'OK') {
                            _this.$('#loginName+span').css('backgroundImage', 'url(' + msg.data + ')');
                        }
                        else if(msg.status === 'ERROR') {
                            _this.$('#loginName+span').css('backgroundImage', 'url()');
                            _this.showLoginErrorMsg(msg.data);
                        }
                    })
                    .fail(function(xhr, textStatus, errorThrown) {
                        _this.showLoginErrorMsg(xhr.responseJSON.data);
                    });
            }
        },

        //
        loginUser: function() {
            if(this.$('#loginMsg').html().length === 0) {
                var _this = this,
                    loginName = this.$('#loginName').val(),
                    loginPassword = this.$('#loginPassword').val();
                if(loginName && loginPassword) {
                    var formData = {loginName: loginName, loginPassword: loginPassword};
                    Util.requestJSON('login/loginUser', formData)
                        .done(function(msg) {  //login OK
                            if(msg.status === 'OK_REDIRECT') {
                                window.location.replace(msg.data);
                            }
                            else if(msg.status === 'ERROR') {
                                _this.showLoginErrorMsg(msg.data);
                            }
                        })
                        .fail(function(xhr, textStatus, errorThrown) {
                            _this.showLoginErrorMsg(xhr.responseJSON.data);
                        });
                }
            }
        },

        //
        checkKeyup: function(event) {
            var keycode = event.keyCode || event.which,
                $loginMsg = this.$('#loginMsg');
            if(keycode === enterKey) {
                this.loginUser();
            }
            else if(keycode !== tabKey && $loginMsg.html().length > 0) {
                $loginMsg.empty();
            }
        },

        //
        openRegistration: function(event) {
            event.preventDefault();
            this.$('#formRegWrap').toggleClass('regOpened');
        },

        //
        loadUserRegImg: function(event) {
            Util.loadImg(event, 36)
                .done(function(imageData) {
                    $(event.target).siblings('span.loadedImg')
                        .css('background-image', 'url(' + imageData + ')');
                });
        },

        //
        registerUser: function() {
            if(this.$('#regMsg').html().length === 0) {
                var _this = this,
                    formData = this.getRegFormData();
                if(this.validateRegFormData(formData)) {
                    Util.requestJSON('login/registerUser', formData)
                        .done(function(msg) {   //register OK
                            if(msg.status === 'OK_REDIRECT') {
                                window.location.replace(msg.data);
                            }
                            else if(msg.status === 'ERROR') {
                                _this.showRegErrorMsg(msg.data);
                            }
                        })
                        .fail(function(xhr, textStatus, errorThrown) {
                            _this.showRegErrorMsg(xhr.responseJSON.data);
                        });
                }
            }
        },

        //
        getRegFormData: function() {
            var formData = {};

            this.$('#formReg').find('input, textarea').each(function() {
                var value = null;
                if($(this).attr('name') === 'userRegImg') {
                    value = $(this).siblings('span.loadedImg').css('background-image')
                        .replace('url(', '').replace(')', '').replace(/^"(.*)"$/, '$1');
                }
                else {
                    value = $(this).val() || null;
                }
                formData[$(this).attr('name')] = value;
            });

            return formData;
        },

        //
        validateRegFormData: function(formData) {
            var errorMsg = null;
            if(!formData['regName']) {
                errorMsg = 'укажите логин';
            }
            else if(!formData['regFirstname'] || !formData['regLastname']) {
                errorMsg = 'укажите имя/фамилию';
            }
            else if(!formData['regPassword'] || !formData['regPassword2']) {
                errorMsg = 'укажите пароль';
            }
            else if(!formData['regKey']) {
                errorMsg = 'укажите ключ';
            }

            if(errorMsg) {
                this.showRegErrorMsg(errorMsg);
                return false;
            }
            return true;
        },

        //
        showLoginErrorMsg: function(msg) {
            this.$('#loginMsg').html(msg);
        },

        //
        showRegErrorMsg: function(msg) {
            this.$('#regMsg').html(msg);
        }

    });
    new login.LayoutLogin();


    //
    window.onerror = function(message, source, line, column, error) {
        Monitor.logJsError(message, source, error);
    };


    //
    login.getAjaxRequestDescription = function(settings) {
        var action = settings.url.slice(settings.url.indexOf('/') + 1),
            actionList = {
                'checkUserName': 'проверка пользователя',
                'loginUser': 'вход',
                'registerUser': 'регистрация'
            };
        return actionList[action];
    };


    return login;

})(window, jQuery, Backbone, _);
