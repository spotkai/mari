<?php
error_reporting(0);
define('ROOT_PATH', 'https://' . $_SERVER['HTTP_HOST'] . '/mari'); //todo remove! '/mari' (for localhost debug only)

header('X-Frame-Options: sameorigin');
header('Strict-Transport-Security: max-age=31536000; includeSubDomains');  //todo enable for HTTPS
header('Referrer-Policy: origin');
header('X-Content-Type-Options: nosniff');
header('Content-Type: text/html; charset=utf-8');
header('X-XSS-Protection: 1; mode=block');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: -1');
header('X-Permitted-Cross-Domain-Policies: master-only');
header("Content-Security-Policy: script-src 'self' https://ajax.googleapis.com; img-src 'self' data:; connect-src 'self' https://ajax.googleapis.com");


//default controller & action
$controllerName = 'login';
$actionName = 'default';
$excludeAccessControllers = ['Controller_login', 'Controller_error', 'Controller_monitor'];

$routes = $_SERVER['REQUEST_URI'];
if(($pos = strrpos($routes, '?')) !== false) {
    $routes = substr($routes, 0, $pos);
}
$routes = explode('/', $routes);
array_shift($routes); //todo remove line! (for localhost debug only)

if(!empty($routes[1])) {
    $controllerName = $routes[1];
}
if(!empty($routes[2]) && $controllerName !== 'error') {
    $actionName = $routes[2];
}

$controllerName = 'Controller_' . $controllerName;
$actionName = 'action_' . $actionName;
$controllerPath = 'controllers/' . $controllerName . '.php';

if(!file_exists($controllerPath)) {
    _errorRedirect('404#');
}
include_once $controllerPath;
$controller = new $controllerName();

if(!method_exists($controller, $actionName)) {
    _errorRedirect('405#');
}
if(!in_array($controllerName, $excludeAccessControllers, true)) {
    $controller->checkAccess();
}
$controller->$actionName();


//
function _errorRedirect($errorCode) {
    header('Location: ' . ROOT_PATH . '/error/' . $errorCode);
    exit();
}