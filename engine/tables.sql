USE `u114412620_mp`;

DROP TABLE IF EXISTS `locked`;
CREATE TABLE `locked` (
  `projectId` mediumint(9) unsigned NOT NULL,
  `userName` varchar(30) NOT NULL,
  `lockTime` datetime DEFAULT NULL,
  PRIMARY KEY (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `projectId` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `projectName` varchar(12) NOT NULL,
  `projectState` varchar(20) DEFAULT NULL,
  `projectImg` text,
  `projectParams` text,
  `projectTasks` text,
  `projectData` text,
  `projectDateStart` date DEFAULT NULL,
  `projectDateFinish` date DEFAULT NULL,
  `projectSpec` mediumtext,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`projectId`),
  UNIQUE KEY `projectName_UNIQUE` (`projectName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userId` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(30) NOT NULL,
  `userPassword` varchar(60) NOT NULL,
  `userCode` varchar(30) NOT NULL,
  `userProfile` binary(5) NOT NULL DEFAULT '00000',
  `userAvatar` text,
  `userLastVisit` datetime DEFAULT NULL,
  `userTryCount` smallint(6) DEFAULT '0',
  `userHash` varchar(60) DEFAULT NULL,
  `userTasks` text,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userName_UNIQUE` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `contactId` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `contactSurname` varchar(30) DEFAULT NULL,
  `contactName` varchar(30) DEFAULT NULL,
  `contactJob` varchar(30) DEFAULT NULL,
  `contactCompany` varchar(50) DEFAULT NULL,
  `contactTel` text,
  `contactInet` text,
  `contactAddress` varchar(120) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
