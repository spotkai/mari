<?php

class Api {

    private $username = 'admin';
    private $password = '7294elley3886';
    private $hostname = 'localhost:3306';
    private $defaultDB = 'mp_work';

    protected $connection = false;
    protected $method = '';
    protected $id = null;


    //
    protected function __construct() {
        $this->method = $_SERVER['REQUEST_METHOD'];
        if($this->method === 'PUT' || $this->method === 'PATCH' || $this->method === 'DELETE') {
            $params = explode('/', rtrim($_SERVER['REQUEST_URI'], '/'));
            $this->id = array_pop($params);
        }
    }


    //
    protected function processAPI() {
    }


    //
    protected function _response($status, $data = null, $statusCode = 200) {
        http_response_code($statusCode);
        header('Content-Type: application/json; charset=utf-8');
        exit(json_encode(['status' => $status, 'data' => $data], JSON_UNESCAPED_UNICODE));
    }


    //
    protected function dbConnect() {
        if(!$this->connection) {
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            try {
                $this->connection = new mysqli($this->hostname, $this->username, $this->password, $this->defaultDB);
                $this->connection->set_charset("utf8");
            }
            catch(Exception $e) {
                $errorMsg = 'Ошибка подключения к базе данных.';
                $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
                if($isAjax) {
                    $this->_response("ERROR", $errorMsg, 503);
                }
                else {
                    header('Location: ' . ROOT_PATH . '/error/503#');
                    exit();
                }
            }
        }
    }


    //
    protected function dbDisconnect() {
        if($this->connection) {
            $this->connection->close();
            $this->connection = false;
        }
    }


    //
    protected function getUserHash($userPassword, $userCode) {
        $userBrowser = substr($_SERVER['HTTP_USER_AGENT'], -31);
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userIP = substr($userIP, 0, strrpos($userIP, '.'));
        $userCode = substr($userCode, -31);
        $hash = crypt($userIP . $userPassword . $userBrowser, $userCode);  //72 chars input max
        return $hash;
    }


    //
    protected function getUserToken($loginName) {
        $userBrowser = substr($_SERVER['HTTP_USER_AGENT'], -51);
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userIP = substr($userIP, 0, strrpos($userIP, '.'));
        $token = md5($userIP . $userBrowser . $loginName);
        return $token;
    }


    //
    protected function genPass($pass) {
        $userCode = '$2a$10$' . substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()))), 0, 22) . '$';
        $userPassword = crypt($pass, $userCode);
        return ['userCode' => $userCode, 'userPassword' => $userPassword];
    }


    //
    protected function setLoginCookies($loginName, $userPassword, $userCode) {
        $userHash = $this->getUserHash($userPassword, $userCode);
        $token = $this->getUserToken($loginName);
        setcookie("MPID", $loginName, time() + 3600 * 24 * 3, '/', null, null, true); //3 days
        setcookie("MPHASH", $userHash, time() + 3600 * 24 * 3, '/', null, null, true);
        setcookie("MPT", $token, time() + 3600 * 24 * 3, '/', null, null, true); //true, true)
        return $userHash;
    }


    //
    protected function clearIdentity() {
        if(isset($_COOKIE['MPID'])) {
            $userName = $_COOKIE['MPID'];
            $this->dbConnect();
            try {
                $sql = 'UPDATE users SET userHash=NULL WHERE userName=?';
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param('s', $userName);
                $stmt->execute();
                $stmt->close();
                $this->dbDisconnect();
            }
            catch(Exception $e) {
                $errorMsg = 'Database error: ' . $e->getMessage();
                $this->_response("ERROR", $errorMsg, 500);
            }
        }

        unset($_COOKIE['MPID']);
        unset($_COOKIE['MPHASH']);
        unset($_COOKIE['MPT']);
        setcookie('MPID', '', time() - 3600, '/', null, null, true);  //true, true)
        setcookie('MPHASH', '', time() - 3600, '/', null, null, true);
        setcookie('MPT', '', time() - 3600, '/', null, null, true);
    }


    //
    public function checkAccess() {
        if(isset($_COOKIE['MPID']) && isset($_COOKIE['MPHASH']) && isset($_COOKIE['MPT'])
            && $this->getUserToken($_COOKIE['MPID']) === $_COOKIE['MPT']
        ) {
            return true;
        }

        include_once('controllers/Controller_monitor.php');
        Controller_monitor:: logAccessError();

        $this->clearIdentity();

        $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        if($isAjax) {
            $this->_response('ERROR_REDIRECT', 'Unauthorized', 401);
        }
        else {
            header('Location: ' . ROOT_PATH . '/error/401#');
            exit();
        }
    }

}