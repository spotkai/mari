<?php

class Controller_monitor {

    public function __construct() {

    }


    //
    public function action_logExecutionTime() {

    }


    //
    public function action_logJsError() {
        $data = json_decode(file_get_contents('php://input'), true);
        date_default_timezone_set("Europe/Minsk");
        $browser = get_browser(null, true);

        if($fp = @fopen("js-error.log", "a")) {
            $logLine = " [{$_COOKIE['MPID']} / {$browser['browser']} / {$browser['platform']}]: {$data['message']}\n"
                . "\tin {$data['source']}\n"
                . " [{$data['stack']}]\n\n";
            fwrite($fp, date("M d H:i:s") . $logLine);
            fclose($fp);
        }
    }


    //
    public static function logAccessError() {
        date_default_timezone_set("Europe/Minsk");
        $url = $_SERVER['REQUEST_URI'];
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userID = $_COOKIE['MPID'];

        if($fp = @fopen("access-error.log", "a")) {
            $logLine = " Unauthorized user: [{$userID} / {$userIP} / {$url}]\n\n";
            fwrite($fp, date("M d H:i:s") . $logLine);
            fclose($fp);
        }
    }


    //
    public static function logAuthError($login, $try) {
        date_default_timezone_set("Europe/Minsk");
        $userIP = $_SERVER['REMOTE_ADDR'];

        if($fp = @fopen("access-error.log", "a")) {
            $logLine = " Wrong password ({$try} tries): [{$login} / {$userIP}]\n\n";
            fwrite($fp, date("M d H:i:s") . $logLine);
            fclose($fp);
        }
    }

}