<?php

class Controller_error {

    //
    function action_default() {
        $pos = strrpos($_SERVER['REQUEST_URI'], '/');
        $errorCode = +substr($_SERVER['REQUEST_URI'], $pos + 1);
        if(strlen($errorCode) !== 3) {
            $errorCode = 500;
        }
        $codesRus = [
            401 => 'Вы не авторизованы.',
            403 => 'Доступ к ресурсу запрещен.',
            404 => 'Запрашиваемый ресурс не найден.',
            405 => 'Недопустимый метод.',
            408 => 'Истекло время ожидания сервером запроса.',
            500 => 'Непредвиденная внутренняя ошибка сервера.',
            503 => 'Сервис недоступен.</br>Ошибка базы данных.'
        ];

        if($errorCode === 401) {
            header('Location: ' . ROOT_PATH . '/login');
            exit();
        }

        $errorMsg = $codesRus[$errorCode];
        http_response_code($errorCode);
        include('views/layout_error.php');
    }

}