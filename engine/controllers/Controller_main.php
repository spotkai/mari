<?php
require_once 'Api.php';

class Controller_main extends Api {

    public function __construct() {
        parent::__construct();
    }


    //
    function action_default() {
        if($this->enterAuth()) {
            include('views/layout_main.php');
        }
        else {
            header('Location: ' . ROOT_PATH . '/error/401#');
            exit();
        }
    }


    //
    function enterAuth() {
        $userName = $_COOKIE['MPID'];
        $this->dbConnect();
        try {
            $sql = 'SELECT userPassword, userCode, userHash FROM users WHERE userName=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $userName);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows === 1) {
                $stmt->bind_result($userPassword, $userCode, $userHash);
                $stmt->fetch();
                if($userHash === $_COOKIE['MPHASH'] && $userHash === $this->getUserHash($userPassword, $userCode)) {
                    $stmt->free_result();
                    date_default_timezone_set("Europe/Minsk");
                    $now = date('Y-m-d H:i:s');

                    $sql = 'UPDATE users SET userLastVisit=? WHERE userName=?';
                    $stmt = $this->connection->prepare($sql);
                    $stmt->bind_param('ss', $now, $userName);
                    $stmt->execute();

                    $stmt->close();
                    $this->dbDisconnect();
                    return true;
                }
            }

            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';

            if($isAjax) {
                $this->_response("ERROR", $errorMsg, 500);
            }
            else {
                header('Location: ' . ROOT_PATH . '/error/500#');
                exit();
            }
        }

        $this->clearIdentity();
        return false;
    }


    //
    function action_logout() {
        $this->clearIdentity();
        header('Location: ' . ROOT_PATH . '/login');
        exit();
    }

}