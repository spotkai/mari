<?php
require_once 'Api.php';

class Controller_userPanel extends Api {

    function __construct() {
        parent::__construct();
    }


    //
    function action_rest() {
        $data = [];
        switch($this->method) {
            case 'GET':
                $data = $this->getUserData();
                break;
            case 'PUT':
                $this->updateUserData();
                break;
            case 'PATCH':
                $this->updateUserTasks();
                break;
            default:
                $this->_response("ERROR", null, 405);
        }
        $this->_response("OK", $data);
    }


    //
    function getUserData() {
        $userName = $_COOKIE['MPID'];
        $sql = 'SELECT userId, userAvatar, userTasks FROM users WHERE userName=? LIMIT 1';
        $this->dbConnect();
        try {
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $userName);
            $stmt->execute();
            $stmt->bind_result($userId, $userAvatar, $userTasks);
            $stmt->fetch();
            $stmt->close();

            $sql = 'SELECT userId, userFirstname, userLastname FROM users';
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($byUserId, $userFirstname, $userLastname);
            $allUsers = [];
            while($stmt->fetch()) {
                $allUsers[$byUserId] = $userFirstname . ' ' . $userLastname;
            }

            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
        if($userAvatar) {
            return [
                'id' => $userId,
                'userName' => $userName,
                'userAvatar' => $userAvatar,
                'userTasks' => json_decode($userTasks, true),
                'allUsers' => $allUsers
            ];
        }
        else {
            return 'Неверный логин2';
        }
    }


    //
    function updateUserData() {
        $data = json_decode(file_get_contents('php://input'), true);
        $userName = $data['userName'];
        $currentPassword = $data['currentPassword'];
        $inputPassword = $data['inputPassword'];
        $userAvatar = $data['userAvatar'];
        $userId = $this->id;

        if(!$currentPassword) {
            $this->_response("ERROR", "Не указан текущий пароль.");
        }

        $this->dbConnect();
        try {
            $sql = 'SELECT userPassword, userCode FROM users WHERE userId=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('i', $userId);
            $stmt->execute();
            $stmt->bind_result($userPassword, $userCode);
            $stmt->fetch();
            $stmt->close();

            if(crypt($currentPassword, $userCode) !== $userPassword) {
                $this->dbDisconnect();
                $this->_response("ERROR", "Неверный текущий пароль.");
            }

            if($inputPassword && $userAvatar) {
                if(strlen($inputPassword) < 3) {
                    $this->_response("ERROR", "Длина нового пароля менее 3 символов.");
                }
                $pass = $this->genPass($inputPassword);
                $newPassword = $pass['userPassword'];
                $newCode = $pass['userCode'];
                $newHash = $this->setLoginCookies($userName, $newPassword, $newCode);

                $sql = "UPDATE users SET userPassword=?, userCode=?, userAvatar=?, userHash=? WHERE userId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("ssssi", $newPassword, $newCode, $userAvatar, $newHash, $userId);
            }
            elseif($userAvatar) {
                $sql = "UPDATE users SET userAvatar=? WHERE userId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("si", $userAvatar, $userId);
            }
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
        $this->_response("OK", 'Настройки были сохранены.');
    }


    //
    function updateUserTasks() {
        $data = json_decode(file_get_contents('php://input'), true);
        $userTasks = json_encode($data['userTasks'], JSON_UNESCAPED_UNICODE);
        $userId = $this->id;

        $this->dbConnect();
        try {
            $sql = "UPDATE users SET userTasks=? WHERE userId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $userTasks, $userId);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }

}
