<?php
require_once 'Api.php';

class Controller_conts extends Api {

    public function __construct() {
        parent::__construct();
    }

    //
    function action_rest() {
        $data = [];
        switch($this->method) {
            case 'GET':
                $data = $this->getContacts();
                break;
            case 'POST':
                $this->addContact();
                break;
            case 'PATCH':
                $this->updateContact();
                break;
            case 'DELETE':
                $this->deleteContact();
                break;
            default:
                $this->_response("ERROR", null, 405);
        }
        $this->_response("OK", $data);
    }


    //
    public function getContacts() {
        $this->dbConnect();
        try {
            $sql = "SELECT contactId, contactSurname, contactName, contactJob, contactCompany, contactTel,
                       contactInet, contactAddress FROM contacts";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            $stmt->store_result();

            $data = [];
            if($stmt->num_rows > 0) {
                $stmt->bind_result($contactId, $contactSurname, $contactName, $contactJob, $contactCompany, $contactTel,
                    $contactInet, $contactAddress);
                $i = 0;
                while($stmt->fetch()) {
                    $data[$i]['id'] = $contactId;
                    $data[$i]['contactSurname'] = $contactSurname;
                    $data[$i]['contactName'] = $contactName;
                    $data[$i]['contactJob'] = $contactJob;
                    $data[$i]['contactCompany'] = $contactCompany;
                    $data[$i]['contactTel'] = json_decode($contactTel, true);
                    $data[$i]['contactInet'] = json_decode($contactInet, true);
                    $data[$i]['contactAddress'] = $contactAddress;
                    $i++;
                }
            }

            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
        return $data;
    }


    //
    public function addContact() {
        $data = json_decode(file_get_contents('php://input'), true);
        $contactSurname = $data['contactSurname'];
        $contactName = $data['contactName'];
        $contactJob = $data['contactJob'];
        $contactCompany = $data['contactCompany'];
        $contactTel = json_encode($data['contactTel'], JSON_UNESCAPED_UNICODE);
        $contactInet = json_encode($data['contactInet'], JSON_UNESCAPED_UNICODE);
        $contactAddress = $data['contactAddress'];

        $this->checkContactExists($contactSurname, $contactName, $contactCompany);

        $this->dbConnect();
        try {
            $sql = "INSERT INTO contacts (contactSurname, contactName, contactJob, contactCompany, contactTel,
                  contactInet, contactAddress) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("sssssss", $contactSurname, $contactName, $contactJob, $contactCompany, $contactTel,
                $contactInet, $contactAddress);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }


    //
    public function updateContact() {
        $data = json_decode(file_get_contents('php://input'), true);
        $contactSurname = @$data['contactSurname'];
        $contactName = $data['contactName'];
        $contactCompany = @$data['contactCompany'];
        $contactId = $this->id;

        $this->checkContactExists($contactSurname, $contactName, $contactCompany, +$contactId);

        $this->dbConnect();
        try {
            if($contactSurname || $contactCompany) {
                $contactJob = $data['contactJob'];
                $contactTel = json_encode($data['contactTel'], JSON_UNESCAPED_UNICODE);
                $contactInet = json_encode($data['contactInet'], JSON_UNESCAPED_UNICODE);
                $contactAddress = $data['contactAddress'];

                $sql = "UPDATE contacts SET contactSurname=?, contactName=?, contactJob=?, contactCompany=?, contactTel=?,
                          contactInet=?, contactAddress=? WHERE contactId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("sssssssi", $contactSurname, $contactName, $contactJob, $contactCompany, $contactTel,
                    $contactInet, $contactAddress, $contactId);
            }

            if(isset($stmt)) {
                $stmt->execute();
                $stmt->close();
                $this->dbDisconnect();
            }
            else {
                $this->dbDisconnect();
                $this->_response("ERROR", 'Ошибка передачи данных.', 500);
            }
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }


    //
    private function checkContactExists($contactSurname, $contactName, $contactCompany, $contactId = null) {
        $this->dbConnect();
        try {
            $sql = 'SELECT contactId FROM contacts
                      WHERE contactSurname<=>? AND contactName<=>? AND contactCompany<=>?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('sss', $contactSurname, $contactName, $contactCompany);
            $stmt->execute();
            $stmt->bind_result($extContactId);
            $stmt->fetch();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        if($extContactId && $extContactId !== 'null' && $extContactId !== $contactId) {
            $errorMsg = 'Контакт "' . $contactSurname . ' ' . $contactName . ' / ' . $contactCompany . '" уже существует.';
            $this->_response("ERROR", $errorMsg);
        }
    }


    //
    public function deleteContact() {
        $contactId = $this->id;
        $this->dbConnect();

        try {
            $sql = "DELETE FROM contacts WHERE contactId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("i", $contactId);
            $stmt->execute();
            $affectedRows = $stmt->affected_rows;
            $stmt->close();
            $this->dbDisconnect();

            if($affectedRows > 0) {
                $this->_response("OK", "Контакт был удален.");
            }
            else {
                $this->_response("ERROR", "Ошибка при удалении контакта.");
            }
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }

}