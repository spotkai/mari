<?php
require_once 'Api.php';

class Controller_login extends Api {

    function __construct() {
        parent::__construct();
    }


    //
    function action_default() {
        if(isset($_COOKIE['MPID']) && isset($_COOKIE['MPHASH']) && isset($_COOKIE['MPT'])) {
            header('Location: ' . ROOT_PATH . '/main');
            exit();
        }
        else {
            include('views/layout_login.php');
        }
    }


    //
    function action_loginUser() {
        time_nanosleep(0, 800 * 1000000);  //800 ms
        $data = json_decode(file_get_contents('php://input'), true);
        $loginName = preg_replace('/[^A-Za-z0-9_\-]/', '', $data['loginName']);
        $loginPassword = preg_replace('/[^A-Za-z0-9_\-]/', '', $data['loginPassword']);
        if("" === $loginName || "" === $loginPassword) {
            $this->_response("ERROR", "Не указан логин/пароль.");
        }

        $this->dbConnect();
        try {
            $sql = 'SELECT userPassword, userCode, userLastVisit, userTryCount, userHash FROM users WHERE userName=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $loginName);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows === 1) { //userName true
                date_default_timezone_set("Europe/Minsk");
                $stmt->bind_result($userPassword, $userCode, $userLastVisit, $userTryCount, $userHash);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();

                $tryLimit = 5;
                if($userTryCount > 5) {
                    $timeLimit = 1; //min
                }
                else {
                    $timeLimit = 5; //min
                }
                $timeLeft = $timeLimit - (time() - strtotime($userLastVisit)) / 60;
                if($userTryCount >= $tryLimit && $timeLeft > 0) {
                    include_once('controllers/Controller_monitor.php');
                    Controller_monitor:: logAuthError($loginName, $userTryCount);
                    $this->dbDisconnect();
                    $this->_response("ERROR", $userTryCount . ' неправильных попыток.</br>Вход блокирован на ' . ceil($timeLeft) . ' мин.');
                }

                $now = date('Y-m-d H:i:s');
                $sql = 'UPDATE users SET userLastVisit=?, userTryCount=?, userHash=? WHERE userName=?';
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param('siss', $now, $userTryCount, $userHash, $loginName);
                if(crypt($loginPassword, $userCode) === $userPassword) { //password true
                    $userTryCount = 0;
                    $userHash = $this->setLoginCookies($loginName, $userPassword, $userCode);
                    $stmt->execute();
                    $stmt->close();
                    $this->dbDisconnect();
                    $this->_response('OK_REDIRECT', 'main');
                }

                else {
                    $userTryCount += 1;
                    $userHash = null;
                    $stmt->execute();
                    $stmt->close();
                    $this->dbDisconnect();
                    $this->_response("ERROR", "Неверный пароль.");
                }
            }
            else {
                $stmt->free_result();
                $stmt->close();
                $this->dbDisconnect();
                $this->_response("ERROR", "Неверный логин.");
            }
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }


    //
    function action_checkUserName() {
        time_nanosleep(0, 800 * 1000000);  //800 ms
        $data = json_decode(file_get_contents('php://input'), true);
        $userName = $data['loginName'];
        $sql = 'SELECT userAvatar FROM users WHERE userName=?';
        $this->dbConnect();
        try {
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $userName);
            $stmt->execute();
            $stmt->bind_result($userAvatar);
            $stmt->fetch();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
        if($userAvatar) {
            $this->_response('OK', $userAvatar);
        }
        else {
            $this->_response('ERROR', 'Неверный логин.');
        }
    }


    //
    function action_registerUser() {
        time_nanosleep(0, 800 * 1000000);  //800 ms
        $key = '1234512345';
        $data = json_decode(file_get_contents('php://input'), true);
        $userName = preg_replace('/[^A-Za-z0-9_\-]/', '', $data['regName']);
        $regPassword = preg_replace('/[^A-Za-z0-9_\-]/', '', $data['regPassword']);
        $regPassword2 = preg_replace('/[^A-Za-z0-9_\-]/', '', $data['regPassword2']);
        $userAvatar = $data['userRegImg'];
        $regFirstname = preg_replace('/[^\p{Cyrillic}A-Za-z0-9_\-]/u', '', $data['regFirstname']);
        $regLastname = preg_replace('/[^\p{Cyrillic}A-Za-z0-9_\-]/u', '', $data['regLastname']);
        $regKey = preg_replace('/[^A-Za-z0-9_\-]/', '', $data['regKey']);

        if("" === $userName || "" === $regPassword || "" === $regPassword2) {
            $this->_response("ERROR", "Не указан логин/пароль.");
        }
        if("" === $regFirstname || "" === $regLastname) {
            $this->_response("ERROR", "Не указаны имя/фамилия.");
        }
        if($regPassword !== $regPassword2) {
            $this->_response("ERROR", "Пароли не совпадают.");
        }
        if(strlen($userName) < 3 || strlen($regPassword) < 3) {
            $this->_response("ERROR", "Длина логина/пароля менее 3 символов.");
        }
        if($regKey !== $key) {
            $this->_response("ERROR", "Неверный ключ.");
        }

        $this->dbConnect();
        try {
            $sql = 'SELECT userId FROM users WHERE userName=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $userName);
            $stmt->execute();
            $stmt->bind_result($userId);
            $stmt->fetch();
            $stmt->close();
            if($userId || $userId > 100) {  //temporary limit
                $this->dbDisconnect();
                $this->_response("ERROR", "Такой пользователь уже существует.");
            }

            date_default_timezone_set("Europe/Minsk");
            $now = date('Y-m-d H:i:s');
            $pass = $this->genPass($regPassword);
            $userPassword = $pass['userPassword'];
            $userCode = $pass['userCode'];
            $userHash = $this->setLoginCookies($userName, $userPassword, $userCode);
            $userTasks = json_encode([], JSON_UNESCAPED_UNICODE);

            $sql = "INSERT INTO users (userName, userPassword, userCode, userAvatar, userFirstname, userLastname,
                userLastVisit, userHash, userTasks) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("sssssssss", $userName, $userPassword, $userCode, $userAvatar, $regFirstname, $regLastname,
                $now, $userHash, $userTasks);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
        $this->_response('OK_REDIRECT', 'main');
    }

}