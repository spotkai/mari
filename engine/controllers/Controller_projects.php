<?php
require_once 'Api.php';

class Controller_projects extends Api {

    public function __construct() {
        parent::__construct();
    }


    //
    function action_rest() {
        $data = [];
        switch($this->method) {
            case 'GET':
                $data = $this->getProjects();
                break;
            case 'POST':
                $data = $this->addProject();
                break;
            //            case 'PUT':
            //                break;
            case 'PATCH':
                $data = $this->updateProjectInfo();
                break;
            default:
                $this->_response("ERROR", null, 405);
        }
        $this->_response("OK", $data);
    }


    //
    public function getProjects() {       //fixme LS: by any request (&checkLock too!!) download ALL changed/updated rows -> +set collectionUpdatedTime
        if(!isset($_GET['state']) || !isset($_GET['year'])) {
            $this->_response("ERROR", "Доступ к ресурсу запрещен.", 403);
        }

        return $this->loadProjects([
            'state' => preg_replace('/[^A-Za-z0-9_\-]/', '', $_GET['state']),
            'year' => +preg_replace('/[^0-9]/', '', $_GET['year']),
            'collectionForceUpdate' => @preg_replace('/[^A-Za-z]/', '', $_GET['collectionForceUpdate']) === 'true',
            'collectionUpdatedTime' => @preg_replace('/[^0-9\-\: ]/', '', $_GET['collectionUpdatedTime']) ?: null
        ]);
    }


    //
    private function loadProjects($params) {
        $state = $params['state'];
        $year = $params['year'];
        $collectionForceUpdate = $params['collectionForceUpdate'];
        $collectionUpdatedTime = $params['collectionUpdatedTime'];

        $this->dbConnect();
        try {
            $sql = "SELECT projectId, projectName, projectImg, projectParams, projectTasks, projectData, projectDateStart,
                      projectDateFinish, (projectSpec IS NOT NULL AND projectSpec!='') AS projectSpecLoaded, updated FROM projects WHERE projectState=?";
            if($state === 'work') {
                $sql .= ' AND ?';
            }
            elseif($state === 'query') {
                $result = $this->connection->query("SELECT MIN(YEAR(projectDateStart)) AS minYear FROM projects WHERE projectState='query'");
                $minYear = +$result->fetch_assoc()['minYear'];
                if($year < $minYear) {
                    $this->dbDisconnect();
                    $this->_response("ERROR", "Нет данных по {$year} году.");
                }
                $sql .= ' AND YEAR(projectDateStart)=?';
            }
            elseif($state === 'arch') {
                $result = $this->connection->query("SELECT MIN(YEAR(projectDateFinish)) AS minYear FROM projects WHERE projectState='arch'");
                $minYear = +$result->fetch_assoc()['minYear'];
                if($year < $minYear) {
                    $this->dbDisconnect();
                    $this->_response("ERROR", "Нет данных по {$year} году.");
                }
                $sql .= ' AND YEAR(projectDateFinish)=?';
            }
            $sql .= ' ORDER BY projectId DESC';

            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $state, $year);
            $stmt->execute();
            $stmt->store_result();

            $data = [];
            if($stmt->num_rows > 0) {
                $clients = json_decode(file_get_contents('mp_customers.json'), true);
                $stmt->bind_result($projectId, $projectName, $projectImg, $projectParams, $projectTasks, $projectData,
                    $projectDateStart, $projectDateFinish, $projectSpecLoaded, $updated);
                $i = 0;
                while($stmt->fetch()) {
                    $data[$i]['id'] = $projectId;
                    if($collectionForceUpdate || strtotime($updated) > strtotime($collectionUpdatedTime)) {
                        $data[$i]['projectName'] = $projectName;
                        $data[$i]['projectState'] = $state;
                        $data[$i]['projectImg'] = $projectImg;
                        $data[$i]['projectParams'] = json_decode($projectParams, true);
                        $data[$i]['projectTasks'] = json_decode($projectTasks, true);
                        $data[$i]['projectData'] = json_decode($projectData, true);
                        $data[$i]['projectDateStart'] = $projectDateStart;
                        $data[$i]['projectDateFinish'] = $projectDateFinish;
                        $data[$i]['projectSpecLoaded'] = $projectSpecLoaded;
                        $data[$i]['projectClient'] = '';

                        if(preg_match("/^[0-9]{8}/", $projectName)) {
                            $projectCode = mb_substr($projectName, 8, null, 'UTF-8');
                            @$data[$i]['projectClient'] = mb_substr($clients[$projectCode], 0, 60, 'UTF-8');
                        }
                    }
                    $i++;
                }
            }

            $stmt->free_result();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        date_default_timezone_set("Europe/Minsk");
        return ['collection' => $data, 'collectionUpdatedTime' => date('Y-m-d H:i:s')];
    }


    //
    public function addProject() {
        date_default_timezone_set("Europe/Minsk");
        $dataTemplate = [
            [
                ['groupName' => 'Проектирование'],
                ['title' => 'согласование: 3d / эскизы', 'status' => 'inwork'],
                ['title' => 'согласование: тех. задание', 'status' => 'inwork'],
                ['title' => '3d-проектирование', 'status' => 'inwork'],
                ['title' => 'чертежи: листовые', 'status' => 'inwork'],
                ['title' => 'чертежи: ДСП', 'status' => 'inwork'],
                ['title' => 'чертежи: точеные', 'status' => 'inwork'],
                ['title' => 'чертежи: из трубы', 'status' => 'inwork']
            ],
            [
                ['groupName' => 'Производство'],
                ['title' => 'заказ: листовые', 'status' => 'inwork', 'date' => null],
                ['title' => 'заказ: раскрой ДСП', 'status' => 'inwork', 'date' => null],
                ['title' => 'заказ: фурнитура / комплектующие', 'status' => 'inwork'],
                ['title' => 'заказ: точеные', 'status' => 'inwork', 'date' => null],
                ['title' => 'детали: из листа', 'status' => 'inwork'],
                ['title' => 'детали: доски ДСП', 'status' => 'inwork'],
                ['title' => 'детали: точеные', 'status' => 'inwork'],
                ['title' => 'детали: из трубы', 'status' => 'inwork'],
                ['title' => 'сварка / доработка листовых', 'status' => 'inwork'],
                ['title' => 'покраска', 'status' => 'inwork']
            ],
            [
                ['groupName' => 'Отгрузка'],
                ['title' => 'док: спецификация изделия', 'status' => 'inwork'],
                ['title' => 'док: разметка ДСП', 'status' => 'inwork'],
                ['title' => 'док: инструкции / паспорта', 'status' => 'inwork'],
                ['title' => 'док: упак. документация', 'status' => 'inwork'],
                ['title' => 'комплектование', 'status' => 'inwork'],
                ['title' => 'сборка / упаковка', 'status' => 'inwork'],
                ['title' => 'отгрузка', 'status' => 'inwork', 'date' => null],
                ['title' => 'монтаж', 'status' => 'inwork', 'date' => null],
                ['title' => 'оплата заказа', 'status' => 'inwork', 'date' => null]
            ]
        ];
        $tasksTemplate = [];

        $data = json_decode(file_get_contents('php://input'), true);
        $projectName = $data['projectName'];
        $projectState = $data['projectState'];
        $projectImg = $data['projectImg'];
        $projectParams = json_encode($data['projectParams'], JSON_UNESCAPED_UNICODE);
        $projectTasks = json_encode($tasksTemplate, JSON_UNESCAPED_UNICODE);
        $projectData = json_encode($dataTemplate, JSON_UNESCAPED_UNICODE);
        $projectDateStart = date('Y-m-d H:i:s'); //now
        $projectDateFinish = $data['projectDateFinish'];

        if($projectState === 'arch' && !$projectDateFinish) {
            $projectDateFinish = date('Y-m-d H:i:s');   //now
        }
        $this->checkProjectNameTaken($projectName);
        $this->dbConnect();
        try {
            $sql = "INSERT INTO projects (projectName, projectState, projectImg, projectParams, projectTasks, projectData,
                  projectDateStart, projectDateFinish) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("ssssssss", $projectName, $projectState, $projectImg, $projectParams, $projectTasks, $projectData,
                $projectDateStart, $projectDateFinish);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        return $this->loadProjects([
            'state' => $data['state'],
            'year' => +$data['year'],
            'collectionUpdatedTime' => $data['collectionUpdatedTime'] ?: null
        ]);
    }


    //
    function updateProjectInfo() {
        $data = json_decode(file_get_contents('php://input'), true);
        $projectName = @$data['projectName'];
        $projectState = @$data['projectState'];
        $projectTasks = json_encode(@$data['projectTasks'], JSON_UNESCAPED_UNICODE);
        $projectDataPatch = @$data['projectDataPatch'];
        $projectId = $this->id;

        if($projectName) {
            $this->checkProjectNameTaken($projectName, +$projectId);
        }
        $this->dbConnect();
        try {
            if($projectState && $projectState === 'deleted') {
                $userName = $_COOKIE['MPID'];
                $currentPassword = @preg_replace('/[^A-Za-z0-9_\-]/', '', $data['currentPassword']);
                $this->checkUserPassword($userName, $currentPassword);
                $sql = "UPDATE projects SET projectState=?, projectSpec=NULL WHERE projectId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("si", $projectState, $projectId);
            }

            elseif($projectName && $projectState) {
                $projectImg = $data['projectImg'];
                $projectParams = json_encode($data['projectParams'], JSON_UNESCAPED_UNICODE);
                $projectDateFinish = $data['projectDateFinish'];
                if($projectState === 'arch' && !$projectDateFinish) {
                    $projectDateFinish = date('Y-m-d H:i:s');   //now
                }
                if($projectState === 'arch') {
                    $sql = "UPDATE projects SET projectName=?, projectState=?, projectImg=?, projectParams=?,
                              projectDateFinish=?, projectSpec=NULL WHERE projectId=?";
                }
                else {
                    $sql = "UPDATE projects SET projectName=?, projectState=?, projectImg=?, projectParams=?,
                              projectDateFinish=? WHERE projectId=?";
                }
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("sssssi", $projectName, $projectState, $projectImg, $projectParams, $projectDateFinish, $projectId);
            }

            elseif($projectTasks && $projectTasks !== 'null') {
                $sql = "UPDATE projects SET projectTasks=? WHERE projectId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("si", $projectTasks, $projectId);
            }

            elseif($projectDataPatch && $projectDataPatch !== 'null') {
                $stmt = $this->connection->prepare("SELECT projectData FROM projects WHERE projectId=?");
                $stmt->bind_param("i", $projectId);
                $stmt->execute();
                $stmt->bind_result($projectData);
                $stmt->fetch();
                $stmt->close();

                $projectDataObject = json_decode($projectData, true);
                $projectDataObject[$projectDataPatch['groupName']][$projectDataPatch['pointName']] = $projectDataPatch['point'];
                $projectDataNew = json_encode($projectDataObject, JSON_UNESCAPED_UNICODE);
                $sql = "UPDATE projects SET projectData=? WHERE projectId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("si", $projectDataNew, $projectId);
            }

            if(isset($stmt)) {
                $stmt->execute();
                $affectedRows = $stmt->affected_rows;
                $stmt->close();
                $this->dbDisconnect();
            }
            else {
                $this->dbDisconnect();
                $this->_response("ERROR", 'Ошибка передачи данных.', 500);
            }
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        $resp = $this->loadProjects([
            'state' => $data['state'],
            'year' => +$data['year'],
            'collectionUpdatedTime' => $data['collectionUpdatedTime'] ?: null
        ]);
        if($projectState === 'deleted' && $affectedRows > 0) {
            $stateList = ['work' => 'рабочие', 'query' => 'запросы', 'arch' => 'архив', 'deleted' => 'удаленные'];
            $resp["msg"] = "Проект был перемещен в {$stateList[$projectState]}.";
        }

        return $resp;
    }


    //
    private function checkProjectNameTaken($projectName, $projectId = null) {
        $this->dbConnect();
        try {
            $sql = 'SELECT projectId, projectState FROM projects WHERE projectName=?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $projectName);
            $stmt->execute();
            $stmt->bind_result($extProjectId, $extProjectState);
            $stmt->fetch();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        if($extProjectId && $extProjectId !== 'null' && $extProjectId !== $projectId) {
            $stateCodesRus2 = ['work' => 'рабочих проектах', 'query'=> 'запросах', 'arch' => 'архиве', 'deleted' => 'удалённых'];
            $errorMsg = 'Проект "' . $projectName . '" уже существует и находится в ' . $stateCodesRus2[$extProjectState] . '.';
            $this->_response("ERROR", $errorMsg);
        }
    }


    //
    //    public function action_deleteProject() {
    //        if($this->method !== 'DELETE') {
    //            $this->_response("ERROR", "Доступ к ресурсу запрещен.", 403);
    //        }
    //
    //        $data = json_decode(file_get_contents('php://input'), true);
    //        $delProject = @preg_replace('/[^\p{Cyrillic}A-Za-z0-9_\-]/u', '', $data['delProject']);
    //        $currentPassword = @preg_replace('/[^A-Za-z0-9_\-]/', '', $data['currentPassword']);
    //        $userName = $_COOKIE['MPID'];
    //
    //        if("" === $delProject) {
    //            $this->_response("ERROR", "Не указан проект.");
    //        }
    //
    //        $this->dbConnect();
    //        try {
    //            $this->checkUserPassword($userName, $currentPassword);
    //            $sql = "DELETE FROM projects WHERE projectName=?";
    //            $stmt = $this->connection->prepare($sql);
    //            $stmt->bind_param("s", $delProject);
    //            $stmt->execute();
    //            $affectedRows = $stmt->affected_rows;
    //            $stmt->close();
    //            $this->dbDisconnect();
    //        }
    //        catch(Exception $e) {
    //            $errorMsg = 'Database error: ' . $e->getMessage();
    //            $this->_response("ERROR", $errorMsg, 500);
    //        }
    //
    //        if($affectedRows > 0) {
    //            $this->_response("OK", "Проект '" . $delProject . "' был удален.");
    //        }
    //        else {
    //            $this->_response("ERROR", "Проект '" . $delProject . "' не существует.");
    //        }
    //    }


    //
    private function checkUserPassword($userName, $currentPassword) {
        if("" === $currentPassword) {
            $this->dbDisconnect();
            $this->_response("ERROR", "Не указан пароль.");
        }

        $sql = 'SELECT userPassword, userCode FROM users WHERE userName=?';
        $stmt = $this->connection->prepare($sql);
        $stmt->bind_param('s', $userName);
        $stmt->execute();
        $stmt->bind_result($userPassword, $userCode);
        $stmt->fetch();
        $stmt->close();

        if(crypt($currentPassword, $userCode) !== $userPassword) {
            $this->dbDisconnect();
            $this->_response("ERROR", "Неверный текущий пароль.");
        }
    }


    //
    function action_checkLock() {
        date_default_timezone_set("Europe/Minsk");
        $data = json_decode(file_get_contents('php://input'), true);
        $projectId = +$data['projectId'];
        $state = $data['state'];
        $userId = +$data['userId'];

        $this->dbConnect();
        try {
            $sql = 'SELECT userName, lockTime FROM locked WHERE projectId=? LIMIT 1';
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('i', $projectId);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->num_rows === 1) { //locked
                $stmt->bind_result($userName, $lockTime);
                $stmt->fetch();
                $stmt->free_result();
                $stmt->close();
                $this->dbDisconnect();

                $timeLimit = 12; //min
                $timePassed = (time() - strtotime($lockTime)) / 60;
                if(+$userName === $userId) {
                    $res = 'OK';  //same user
                }
                else if($timePassed > $timeLimit) {   //time elapsed
                    $this->action_freeLock($projectId);
                    $this->lockProject($projectId, $userId);    //lock for new user
                    $res = 'OK';
                }
                else {
                    $errorMsg = +$userName;
                    $this->_response("ERROR_USER", $errorMsg);
                }
            }
            else {
                $stmt->free_result();
                $sql = "SELECT projectName, projectState FROM projects WHERE projectId=?";
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param("i", $projectId);
                $stmt->execute();
                $stmt->bind_result($projectName, $extProjectState);
                $stmt->fetch();
                $stmt->close();
                $this->dbDisconnect();
                if($extProjectState !== $state) {
                    $errorMsg = 'Проект "' . $projectName . '" был перемещен в '
                        . (($extProjectState === "arch") ? 'архив.' : (($extProjectState === "query") ? 'запросы.' : 'рабочие проекты.'));
                    $this->_response("ERROR", $errorMsg);
                }
                else {
                    $this->lockProject($projectId, $userId);
                    $res = 'OK';
                }
            }
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        if($res === 'OK') {
            $resp = $this->loadProjects([
                'state' => $state,
                'year' => +$data['year'],
                'collectionUpdatedTime' => $data['collectionUpdatedTime'] ?: null
            ]);
            $this->_response("OK", $resp);
        }
    }


    //
    function action_freeLock($projectId = null) {
        if(!$projectId) {
            $data = json_decode(file_get_contents('php://input'), true);
            $projectId = $data['projectId'];
        }
        $this->dbConnect();
        try {
            $sql = "DELETE FROM locked WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("i", $projectId);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }


    //
    function lockProject($projectId, $userId) {
        $now = date('Y-m-d H:i:s');
        $this->dbConnect();
        try {
            $sql = "INSERT INTO locked (projectId, userName, lockTime) VALUES (?, ?, ?)";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('iis', $projectId, $userId, $now);
            $stmt->execute();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }
    }


    //
    function action_searchProject() {
        $data = json_decode(file_get_contents('php://input'), true);
        $searchType = $data['searchType'];
        $searchNum = preg_replace('/[^\p{Cyrillic}A-Za-z0-9_\-]/u', '', $data['searchNumber']);

        if($searchType === 'projectCustomerName') {
            $customers = $this->getCustomerCodeByName($searchNum);
            $customersCount = count($customers);
            if($customersCount) {
                $this->_response("OK_SELECT", $customers);
            }
            else {
                $this->_response("OK_MSG", "Ничего не найдено.");
            }
        }

        $consoleRegexp = "КОД ИЗДЕЛИЯ[^}]+value[^}]+{$searchNum}[^}]*}";
        $sql = 'SELECT projectId, projectName, projectState, projectImg, projectParams,
                   YEAR(projectDateStart) AS projectStart, YEAR(projectDateFinish) AS projectFinish FROM projects';
        $this->dbConnect();
        try {
            if($searchType === 'projectConsole') {
                $sql .= ' WHERE upper(projectParams) REGEXP ? ORDER BY projectState DESC, projectDateStart DESC';
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param('s', $consoleRegexp);
            }
            elseif($searchType === 'projectCustomer') {
                $searchNumber = "%{$searchNum}";
                $sql .= ' WHERE upper(projectName) LIKE ? ORDER BY projectState DESC, projectDateStart DESC';
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param('s', $searchNumber);
            }
            else {
                $searchNumber = "%{$searchNum}%";
                $sql .= ' WHERE upper(projectName) LIKE ? OR upper(projectParams) REGEXP ? ORDER BY projectState DESC, projectDateStart DESC';
                $stmt = $this->connection->prepare($sql);
                $stmt->bind_param('ss', $searchNumber, $consoleRegexp);
            }

            $stmt->execute();
            $stmt->bind_result($projectId, $projectName, $projectState, $projectImg, $projectParams, $projectStart, $projectFinish);

            $searchResult = [];
            while($stmt->fetch()) {
                if($projectId && $projectId !== 'null') {
                    preg_match("/код изделия.*?value.*?:(.*?)}/i", $projectParams, $matches);
                    $console = trim($matches[1], '"');
                    if($console === 'null') {
                        $console = null;
                    }

                    if($projectState === 'arch') {
                        $projectYear = $projectFinish;
                    }
                    else {
                        $projectYear = $projectStart;
                    }

                    $res = ['projectId' => $projectId,
                        'projectName' => $projectName,
                        'projectState' => $projectState,
                        'projectImg' => $projectImg,
                        'projectConsole' => $console,
                        'projectYear' => $projectYear];
                    array_push($searchResult, $res);
                }
            }
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        if(count($searchResult)) {
            $this->_response("OK", $searchResult);
        }
        else {
            $resMsg = "Ничего не найдено.";
            $this->_response("OK_MSG", $resMsg);
        }
    }


    //
    private function getCustomerCodeByName($customerName) {
        $clients = json_decode(file_get_contents('mp_customers.json'), true);
        return array_filter($clients, function ($v) use ($customerName) {
            return (mb_stripos($v, $customerName, 0, 'UTF-8') !== false);
        });
    }


    //    //
    //    function action_fixTasks() {
    //        $this->dbConnect();
    //        $sqlSel = 'SELECT projectId FROM projects';
    //        $stmt = $this->connection->prepare($sqlSel);
    //        $stmt->execute();
    //        $stmt->bind_result($id);
    //        $idArray = [];
    //        while($stmt->fetch()) {
    //            array_push($idArray, $id);
    //        }
    //        $stmt->close();
    //
    //        for($j = 0, $size = count($idArray); $j < $size; $j++) {
    //            $projectId = $idArray[$j];
    //            $sqlGet = "SELECT projectTasks FROM projects WHERE projectId=?";
    //            $stmt = $this->connection->prepare($sqlGet);
    //            $stmt->bind_param("i", $projectId);
    //            $stmt->execute();
    //            $stmt->bind_result($projectTasks);
    //            $stmt->fetch();
    //            $stmt->close();
    //
    //
    //            $tasks = json_decode($projectTasks, true);
    //            if(empty($tasks)) {
    //                continue;
    //            }
    //            for($i = 0, $len = count($tasks); $i < $len; $i++) {
    //                if(!isset($tasks[$i]['taskId'])) {
    //                    $tasks[$i]['taskId'] = $i;
    //                }
    //                if(!isset($tasks[$i]['taskHighPriority'])) {
    //                    $tasks[$i]['taskHighPriority'] = false;
    //                }
    //            }
    //
    //
    //            $tasksNew = json_encode($tasks, JSON_UNESCAPED_UNICODE);
    //            $sqlUp = "UPDATE projects SET projectTasks=? WHERE projectId=?";
    //            $stmt = $this->connection->prepare($sqlUp);
    //            $stmt->bind_param("si", $tasksNew, $projectId);
    //            $stmt->execute();
    //            $stmt->close();
    //        }
    //        $this->dbDisconnect();
    //        exit('Done !');
    //    }


    //
    function action_getProjectSpec() {
        $data = json_decode(file_get_contents('php://input'), true);
        $projectId = $data['projectId'];

        $this->dbConnect();
        try {
            $sql = "SELECT projectSpec FROM projects WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('i', $projectId);
            $stmt->execute();
            $stmt->bind_result($projectSpec);
            $stmt->fetch();
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        if($projectSpec && $projectSpec !== 'null') {
            $this->_response('OK', $projectSpec);
        }
        else {
            $this->_response('ERROR', 'Спецификация не загружена.');
        }
    }


    //
    function action_handleProjectSpec() {
        $data = json_decode(file_get_contents('php://input'), true);
        $projectId = $data['projectId'];
        $b64String = @$data['b64String'] ?: null;

        $this->dbConnect();
        try {
            $sql = "UPDATE projects SET projectSpec=? WHERE projectId=?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param("si", $b64String, $projectId);
            $stmt->execute();
            $affectedRows = $stmt->affected_rows;
            $stmt->close();
            $this->dbDisconnect();
        }
        catch(Exception $e) {
            $errorMsg = 'Database error: ' . $e->getMessage();
            $this->_response("ERROR", $errorMsg, 500);
        }

        if($affectedRows > 0) {
            if($b64String) {
                $msg = "Спецификация загружена.";
            }
            else {
                $msg = "Спецификация удалена.";
            }

            $resp = $this->loadProjects([
                'state' => $data['state'],
                'year' => +$data['year'],
                'collectionUpdatedTime' => $data['collectionUpdatedTime'] ?: null
            ]);
            $resp["msg"] = $msg;

            $this->_response("OK", $resp);
        }
    }
}