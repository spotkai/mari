<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <title>Mprod - Вход</title>
    <meta name="description" content="Производство :: вход в систему"/>
    <meta name="robots" content="noindex, nofollow"/>
    <link rel="shortcut icon" type="image/png" href="build/resources/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="build/login/login.min.css"/>
</head>

<body>
<div id="loginWrap">
    <div id="ajaxShadow"></div>

    <div id="formLoginWrap">
        <form id="formLogin" method="POST" action="#">
            <div id="loginTitle">
                <h2>Производство :: вход</h2>
                <div id="ajaxLoader">
                    <span class="loaderMsg"></span>
                </div>
            </div>

            <div class="formItem">
                <label for="loginName">Логин</label>
                <input type="text" id="loginName" name="loginName" required="required">
                <span></span>
            </div>

            <div class="formItem">
                <label for="loginPassword">Пароль</label>
                <input type="password" id="loginPassword" name="loginPassword" required="required">
            </div>

            <div id="loginMsg"></div>

            <div class="formAction">
                <button class="btn" type="button" name="enterLogin">Войти</button>
            </div>

            <a id="openReg" href="#">регистрация</a>
        </form>
    </div>

    <div id="formRegWrap">
        <form id="formReg" method="POST" action="#" autocomplete="off">
            <h3>Создать пользователя</h3>

            <div class="formItem">
                <label for="regName">Логин</label>
                <input type="text" id="regName" name="regName" required="required">
                <span></span>
            </div>

            <div class="formItem">
                <label for="regFirstname">Имя</label>
                <input type="text" id="regFirstname" name="regFirstname" required="required">
                <span></span>
            </div>

            <div class="formItem">
                <label for="regLastname">Фамилия</label>
                <input type="text" id="regLastname" name="regLastname" required="required">
                <span></span>
            </div>

            <div class="formItem">
                <label for="regPassword">Пароль</label>
                <input type="password" id="regPassword" name="regPassword" required="required">
            </div>

            <div class="formItem">
                <label for="regPassword2">Повтор пароля</label>
                <input type="password" id="regPassword2" name="regPassword2" required="required">
            </div>

            <div class="formItem">
                <label for="userRegImg">Аватар</label>
                <input id="userRegImg" type="file" accept="image/*" name="userRegImg">
                <span class="loadedImg"></span>
            </div>

            <div class="formItem">
                <label for="regKey">Ключ</label>
                <input type="text" id="regKey" name="regKey" required="required">
            </div>

            <div id="regMsg"></div>

            <div class="formAction">
                <button class="btn" type="button" name="enterReg">Создать</button>
            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="vendor/underscore-min.js"></script>
<script src="vendor/backbone-min.js"></script>

<script src="build/login/login.min.js"></script>
</body>
</html>