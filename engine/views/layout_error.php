<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <title>Ошибка</title>
    <meta name="description" content="Производство :: страница ошибки"/>
    <meta name="robots" content="noindex, nofollow"/>
    <link rel="shortcut icon" type="image/png" href="../build/resources/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="../build/error/error.min.css"/>
</head>

<body>
<div id="errorWrap">
    <div id="errorTitle">
        <h2><?php echo($errorCode); ?>#</h2>
    </div>

    <div id="errorMsg">
        <h2><?php echo($errorMsg); ?></h2>
    </div>

    <div id="errorFooter">
        <div id="reLogin"><a href="../login">войти в систему</a></div>
        <div id="masterprojectSite"><a href="https://masterproject.by">masterproject.by</a></div>
    </div>
</div>
</body>
</html>