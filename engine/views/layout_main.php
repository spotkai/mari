<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <title>Производство</title>
    <meta name="description" content="Производство :: приложение"/>
    <meta name="robots" content="noindex, nofollow"/>
    <link rel="shortcut icon" type="image/png" href="build/resources/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="vendor/pickadate/picker-classic.css"/>
    <link rel="stylesheet" type="text/css" href="vendor/pickadate/picker-classic-date.css"/>
    <link rel="stylesheet" type="text/css" href="build/main/main.min.css"/>
</head>

<body>
<div id="ajaxShadow"></div>

<header id="mainHeader">
    <div id="appMenu">
        <ul>
            <li><a href="#!projects">Проекты</a></li>
            <li><a href="#!docs">Документы</a></li>
            <li><a href="#!conts">Контакты</a></li>
            <li id="reloadPage"><span></span></li>
        </ul>
    </div>
    
    <div id="ajaxLoader">
        <span class="loaderMsg"></span>
        <span class="loaderTime"></span>
    </div>

    <div id="notice">
        <div id="noticeTitle"></div>
        <div id="noticeContent"></div>
        <div id="noticeCloser" class="hoverCircle"></div>
    </div>
    <div id="noticeShadow"></div>
</header>

<div id="appContainer"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="vendor/underscore-min.js"></script>
<script src="vendor/backbone-min.js"></script>
<script src="vendor/pickadate/picker.min.js"></script>
<script src="vendor/pickadate/picker-date.min.js"></script>

<script src="build/main/main.template.js"></script>
<script src="build/main/main.min.js"></script>
</body>
</html>