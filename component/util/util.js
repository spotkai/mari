//
var Util = (function(window, $) {
    "use strict";

    var util = {},
        xhrPool = [];   //array of current uncompleted requests


    //create mediator obj
    window.vent = {};
    _.extend(window.vent, Backbone.Events);


    //ajax-abort function
    util.abortXhrPool = function() {
        xhrPool.forEach(function(jqXHR, idx) {
            jqXHR.abort();
        });
        xhrPool.length = 0;      //fixme add stop loading-animation?
    };

    //jQ ajax settings
    $.ajaxSetup({
        cache: false,
        timeout: 90 * 1000,
        beforeSend: function(jqXHR, settings) {
            xhrPool.push(jqXHR);
            if(xhrPool.length === 1) {
                vent.trigger('ajaxRequest:start');
            }
            var ajaxRequestDescription = '',
                sender = settings.url.slice(0, settings.url.indexOf('/')),
                senderName = sender.capitalize();

            if(window[senderName]) {
                ajaxRequestDescription = window[senderName].getAjaxRequestDescription(settings);
            }
            $('.loaderMsg').html(ajaxRequestDescription);
        },
        complete: function(jqXHR, textStatus) {
            var index = xhrPool.indexOf(jqXHR);
            if(index > -1) {
                xhrPool.splice(index, 1);
            }
            if(!xhrPool.length) {
                vent.trigger('ajaxRequest:done');
                $('.loaderMsg').empty();
            }
        }
    });

    //
    util.requestJSON = function(URL, requestData, requestType) {
        var type = requestType || 'POST';
        return $.ajax({
            type: type,
            url: URL,
            contentType: 'application/json',
            data: type.toUpperCase() === 'GET' ? requestData : JSON.stringify(requestData)
        });
    };

    //
    util.startLoadingAnimation = function(pos) {
        $('#ajaxLoader').css(pos).addClass('ajaxLoad');
        $('#ajaxShadow').addClass('fog');

        var time0 = new Date().getTime();
        util.timerID = window.setInterval(function() {
            var time1 = new Date().getTime(),
                interval = (time1 - time0) / 1000 ^ 0;
            $('.loaderTime').html(interval || '');
        }, 200);  //0.2 sec refresh
    };
    util.stopLoadingAnimation = function() {
        $('#ajaxLoader').removeClass('ajaxLoad');
        $('#ajaxShadow').removeClass('fog');

        window.clearInterval(util.timerID);
        $('.loaderTime').html('');
    };


    //
    util.getDay = function(taskDate) {
        var dayList = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
        return dayList[new Date(taskDate).getDay()];
    };

    //
    util.prettifyDate = function(taskDate, year) {
        var date = new Date(taskDate);
        if(!taskDate || window.isNaN(date.getTime())) {
            return '...';
        }
        var monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня",
                "июля", "августа", "сентября", "октября", "ноября", "декабря"],
            prettyDate = date.getDate() + ' ' + monthNames[date.getMonth()];
        if(year) {
            prettyDate += ' ' + date.getFullYear() + 'г.';
        }
        return prettyDate;
    };

    //
    util.getMonthName = function(month) {
        var monthNames = ["январь", "февраль", "март", "апрель", "май", "июнь",
            "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"];
        return monthNames[month - 1];
    };

    //get current date in (yyyy-MM-dd HH:mm:ss) format
    util.getDate = function(hour) {
        var now = new Date(),
            yyyy = now.getFullYear(),
            MM = now.getMonth() + 1,
            dd = now.getDate();

        if(dd < 10) {
            dd = '0' + dd;
        }
        if(MM < 10) {
            MM = '0' + MM;
        }
        var date = yyyy + '-' + MM + '-' + dd;

        if(hour) {
            var HH = now.getHours(),
                mm = now.getMinutes(),
                ss = now.getSeconds();
            date += ' ' + HH + ':' + mm + ':' + ss;
        }
        return date;
    };

    //current year
    util.getYear = function() {
        return new Date().getFullYear();
    };

    //
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };

    //
    util.loadImg = function(event, size) {
        var def = new $.Deferred(),
            file = event.target.files[0];
        if(!file) {
            def.resolve('data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
            return def.promise();
        }

        var reader = new FileReader();
        reader.onload = function(ev) {
            var MAX_SIZE = size,
                image = new Image();
            image.src = ev.target.result;

            image.onload = function() {
                var imageData;
                if(image.height > MAX_SIZE || image.width > MAX_SIZE) {
                    imageData = pixelDownscale(image, MAX_SIZE, MAX_SIZE);
                }
                else {
                    imageData = ev.target.result;
                }
                def.resolve(imageData);
            };
        };
        reader.readAsDataURL(file);

        return def.promise();
    };

    //pixel perfect scale
    function pixelDownscale(img, W2, H2) {
        var canvas = document.createElement('canvas'),
            sw = canvas.width = img.width,    // source image width
            sh = canvas.height = img.height,  // source image height
            ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);

        if(W2 > 50 || H2 > 50) {  //'contain' pic
            var scale;
            if(img.height > img.width) {
                scale = H2 / img.height;
            }
            else {
                scale = W2 / img.width;
            }
        }
        else {  //'cover' for small pic
            if(img.height > img.width) {
                scale = W2 / img.width;
            }
            else {
                scale = H2 / img.height;
            }
        }

        var sqScale = scale * scale, // square scale = area of source pixel within target
            tw = Math.floor(sw * scale), // target image width
            th = Math.floor(sh * scale); // target image height
        // EDIT (credits to @Enric ) : was ceil before, and creating artifacts :
        //                           var tw = Math.ceil(sw * scale); // target image width
        //                           var th = Math.ceil(sh * scale); // target image height
        var sx = 0, sy = 0, sIndex = 0, // source x,y, index within source array
            tx = 0, ty = 0, yIndex = 0, tIndex = 0, // target x,y, x,y index within target array
            tX = 0, tY = 0, // rounded tx, ty
            w = 0, nw = 0, wx = 0, nwx = 0, wy = 0, nwy = 0; // weight / next weight x / y
        // weight is weight of current source point within target.
        // next weight is weight of current source point within next target's point.
        var crossX = false, // does scaled px cross its current px right border ?
            crossY = false, // does scaled px cross its current px bottom border ?
            sBuffer = ctx.getImageData(0, 0, sw, sh).data, // source buffer 8 bit rgba
            tBuffer = new Float32Array(3 * sw * sh), // target buffer Float32 rgb
            sR = 0, sG = 0, sB = 0; // source's current point r,g,b
        /* untested !
         var sA = 0;  //source alpha  */

        for(sy = 0; sy < sh; sy++) {
            ty = sy * scale; // y src position within target
            tY = 0 | ty;     // rounded : target pixel's y
            yIndex = 3 * tY * tw;  // line index within target array
            crossY = (tY != (0 | ty + scale));
            if(crossY) { // if pixel is crossing botton target pixel
                wy = (tY + 1 - ty); // weight of point within target pixel
                nwy = (ty + scale - tY - 1); // ... within y+1 target pixel
            }
            for(sx = 0; sx < sw; sx++, sIndex += 4) {
                tx = sx * scale; // x src position within target
                tX = 0 | tx;    // rounded : target pixel's x
                tIndex = yIndex + tX * 3; // target pixel index within target array
                crossX = (tX != (0 | tx + scale));
                if(crossX) { // if pixel is crossing target pixel's right
                    wx = (tX + 1 - tx); // weight of point within target pixel
                    nwx = (tx + scale - tX - 1); // ... within x+1 target pixel
                }
                sR = sBuffer[sIndex];   // retrieving r,g,b for curr src px.
                sG = sBuffer[sIndex + 1];
                sB = sBuffer[sIndex + 2];

                /* !! untested : handling alpha !!
                 sA = sBuffer[sIndex + 3];
                 if (!sA) continue;
                 if (sA != 0xFF) {
                 sR = (sR * sA) >> 8;  // or use /256 instead ??
                 sG = (sG * sA) >> 8;
                 sB = (sB * sA) >> 8;
                 }
                 */
                if(!crossX && !crossY) { // pixel does not cross
                    // just add components weighted by squared scale.
                    tBuffer[tIndex] += sR * sqScale;
                    tBuffer[tIndex + 1] += sG * sqScale;
                    tBuffer[tIndex + 2] += sB * sqScale;
                }
                else if(crossX && !crossY) { // cross on X only
                    w = wx * scale;
                    // add weighted component for current px
                    tBuffer[tIndex] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    // add weighted component for next (tX+1) px
                    nw = nwx * scale;
                    tBuffer[tIndex + 3] += sR * nw;
                    tBuffer[tIndex + 4] += sG * nw;
                    tBuffer[tIndex + 5] += sB * nw;
                }
                else if(crossY && !crossX) { // cross on Y only
                    w = wy * scale;
                    // add weighted component for current px
                    tBuffer[tIndex] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    // add weighted component for next (tY+1) px
                    nw = nwy * scale;
                    tBuffer[tIndex + 3 * tw] += sR * nw;
                    tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                    tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                }
                else { // crosses both x and y : four target points involved
                    // add weighted component for current px
                    w = wx * wy;
                    tBuffer[tIndex] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    // for tX + 1; tY px
                    nw = nwx * wy;
                    tBuffer[tIndex + 3] += sR * nw;
                    tBuffer[tIndex + 4] += sG * nw;
                    tBuffer[tIndex + 5] += sB * nw;
                    // for tX ; tY + 1 px
                    nw = wx * nwy;
                    tBuffer[tIndex + 3 * tw] += sR * nw;
                    tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                    tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                    // for tX + 1 ; tY +1 px
                    nw = nwx * nwy;
                    tBuffer[tIndex + 3 * tw + 3] += sR * nw;
                    tBuffer[tIndex + 3 * tw + 4] += sG * nw;
                    tBuffer[tIndex + 3 * tw + 5] += sB * nw;
                }
            } // end for sx
        }

        // create result canvas
        canvas.width = tw;
        canvas.height = th;
        var imgRes = ctx.getImageData(0, 0, tw, th),
            tByteBuffer = imgRes.data;
        // convert float32 array into a UInt8Clamped Array
        var pxIndex = 0; //
        for(sIndex = 0, tIndex = 0; pxIndex < tw * th; sIndex += 3, tIndex += 4, pxIndex++) {
            tByteBuffer[tIndex] = Math.ceil(tBuffer[sIndex]);
            tByteBuffer[tIndex + 1] = Math.ceil(tBuffer[sIndex + 1]);
            tByteBuffer[tIndex + 2] = Math.ceil(tBuffer[sIndex + 2]);
            tByteBuffer[tIndex + 3] = 255;
        }
        // writing result to canvas.
        ctx.putImageData(imgRes, 0, 0);
        return canvas.toDataURL('image/png');
    }


    return util;
})(window, jQuery);