var Projects = (function(window, $, Backbone, _) {
    "use strict";

    var projects = {},   //instance
        projectsModel,   //common projectsModel
        props;           //common props


    //
    var ProjectsModel = Backbone.Model.extend({
        defaults: {
            previousProjectId: null,
            currentModel: null,
            $currentThumb: null,
            currentTask: null,
            currentState: 'work',
            currentYear: Util.getYear(),
            performerFilter: null
        },

        initialize: function() {
            this.projectToSet = null;
            this.on('error', function(model, xhr, options) {
                Main.syncError(model, xhr, options);
            });
        }
    });


    //
    var ProjectsView = Backbone.View.extend({
        el: '#projectsHeader',
        events: {
            'click #addProject': 'showAddProjectDialog',
            'click #selectProjects .dropdownMenu>li': 'selectProjects',
            'click #handleFile .dropdownMenu': 'handleFile',
            'change #uploadProjectSpec': 'uploadProjectSpec',
            'click .searchIcon': 'searchProject',
            'keyup #projectsSearch input': 'checkKeyup',
            'click #yearCarousel': 'changeProjectsYear',
            'change #selectedYear input': 'enterYear'
        },

        initialize: function() {
            setEventHandlers();
            this.setupProjectMenu(); //init setup #projectMenu
            this.showCurrentYear();  //init #yearCarousel
            this.listenTo(this.model, 'change:currentState', this.setupProjectMenu);
            this.listenTo(this.model, 'change:currentYear', this.showCurrentYear);
        },

        //
        showAddProjectDialog: function(event) {
            projects.showProjectDialog($(event.target),
                $(templates['addProject']()), 'Добавить проект',
                function() {
                    projects.projectListView.addProject();
                });
        },

        //
        selectProjects: function(ev) {
            var href = $(ev.currentTarget).find('a').attr('href'),
                state = href.slice(href.indexOf(':') + 1);

            ev.preventDefault();
            if(projectsModel.get('currentState') !== state) {
                projects.projectListView.loadProjects({projState: state, forceUpdate: true});
            }
        },

        //
        handleFile: function(ev) {
            if(!projectsModel.get('currentModel')) {
                return false;
            }

            var href = $(ev.target).attr('href'),
                action = href.slice(href.indexOf(':') + 1);
            ev.preventDefault();

            switch(action) {
                case 'upload':
                    this.$('#uploadProjectSpec').click();
                    break;
                case 'delete':
                    this.deleteProjectSpec();
                    break;
            }
        },

        //
        uploadProjectSpec: function(event) {
            var file = event.target.files[0];
            $(event.target).val('');
            if(!file) {
                return;
            }

            if(file.type !== "application/pdf") {
                Main.showNotice({msg: 'Выберите pdf файл.'});
                return;
            }
            if(file.size > props.PDF_FILE_MAXSIZE * 1024 * 1024) {
                Main.showNotice({msg: 'Размер файла превышает ' + props.PDF_FILE_MAXSIZE + ' Мб.'});
                return;
            }

            var reader = new FileReader();
            reader.onload = function(ev) {
                var b64String = ev.target.result;
                Util.requestJSON("projects/handleProjectSpec", {
                    projectId: projectsModel.get('currentModel').get('id'),
                    b64String: b64String,
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
                })
                    .done(function(response) {
                        if(response.status === 'OK') {
                            projects.projectListView.setProjects(response.data);
                            Main.showNotice({msg: response.data['msg'], type: 'INFORM'});
                        }
                        else if(response.status === 'ERROR') {
                            Main.showNotice({msg: response.data});
                        }
                    });
            };
            reader.readAsDataURL(file);
        },

        //
        deleteProjectSpec: function() {
            if(projectsModel.get('currentModel').get('projectSpecLoaded')) {
                Util.requestJSON("projects/handleProjectSpec", {
                    projectId: projectsModel.get('currentModel').get('id'),
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
                })
                    .done(function(response) {
                        if(response.status === 'OK') {
                            projects.projectListView.setProjects(response.data);
                            Main.showNotice({msg: response.data['msg'], type: 'INFORM'});
                        }
                        else if(response.status === 'ERROR') {
                            Main.showNotice({msg: response.data});
                        }
                    });
            }
        },

        //setup #projectMenu
        setupProjectMenu: function() {
            var currentState = projectsModel.get('currentState');
            $('#selectProjects').find('.statusSelected').html(props.stateCodesRus[currentState].capitalize());

            if(currentState === 'work') {
                this.$('#handleFile').addClass('activeElement');
                this.$('#yearCarousel').removeClass('activeElement');
            }
            else if(currentState === 'query' || currentState === 'arch') {
                this.$('#handleFile').removeClass('activeElement');
                this.$('#yearCarousel').addClass('activeElement');
            }
        },

        //
        showCurrentYear: function() {
            this.$('#selectedYear').find('input').val(projectsModel.get('currentYear'));
        },

        //
        searchProject: function() {
            var _this = this,
                searchNumber = this.$('#projectsSearch').find('input').val().trim().toUpperCase(),
                searchType = 'any';
            if(searchNumber.length < 3) {
                return false;
            }

            if(/^\?+/.test(searchNumber)) {
                searchNumber = searchNumber.slice(searchNumber.lastIndexOf('?') + 1);
                searchType = 'projectConsole';
            }
            else if(/^\*+[\wА-ЯЁ]{2,}$/.test(searchNumber)) {
                var index = searchNumber.lastIndexOf('*');
                searchNumber = searchNumber.slice(index + 1);
                if(index >= 1) {
                    searchType = 'projectCustomerName';
                }
                else {
                    searchType = 'projectCustomer';
                }
            }

            Util.requestJSON('projects/searchProject', {searchNumber: searchNumber, searchType: searchType})
                .done(function(response) {
                    if(response.status === 'OK') {
                        _this.showSearchResult(searchNumber, searchType, response.data);
                    }
                    else if(response.status === 'OK_SELECT') {
                        _this.showSearchSelect(searchNumber, searchType, response.data);
                    }
                    else if(response.status === 'OK_MSG') {
                        Main.showNotice({
                            title: 'Результаты поиска \"' + searchNumber + '\"',
                            msg: response.data,
                            type: 'INFORM',
                            keep: true,
                            width: '330px'
                        });
                    }
                });
        },

        //
        showSearchResult: function(searchNumber, searchType, searchResult) {
            var _this = this;
            Main.showNotice({
                title: 'Результаты поиска \"' + searchNumber + '\" [' + Object.keys(searchResult).length + ']',
                msg: templates['searchList']({searchNumber: searchNumber, searchType: searchType, searchResult: searchResult}),
                type: 'INFORM',
                keep: true,
                width: '450px',
                cb: function(ev) {
                    ev.preventDefault();
                    var searchResultLine = $(ev.target).closest('.searchResultLine')[0];
                    if(searchResultLine && !searchResultLine.classList.contains('search-deleted')) {
                        var searchNum = searchResultLine.getAttribute('data-search-num');
                        _this.jumpToProject(searchResult[searchNum]);
                        Main.hideNotice();
                    }
                }
            });
        },

        //
        showSearchSelect: function(searchNumber, searchType, searchResult) {
            var _this = this;
            Main.showNotice({
                title: 'Результаты поиска \"' + searchNumber + '\" [' + Object.keys(searchResult).length + ']',
                msg: templates['searchList']({searchNumber: searchNumber,  searchType: searchType, searchResult: searchResult}),
                type: 'INFORM',
                keep: true,
                width: '450px',
                cb: function(ev) {
                    ev.preventDefault();
                    var searchResultLine = $(ev.target).closest('.searchResultLine')[0];
                    if(searchResultLine) {
                        var searchCode = searchResultLine.getAttribute('data-search-code');
                        Main.hideNotice(true);
                        _this.$('#projectsSearch').find('input').val('*' + searchCode);
                        _this.$('#projectsSearch').find('.searchIcon').click();
                    }
                }
            });
        },

        //
        jumpToProject: function(searchRes) {
            var modelId = searchRes.projectId,
                modelState = searchRes.projectState,
                projectYear = searchRes.projectYear;

            var projToJump = $('#project_' + modelId);
            if(modelState === projectsModel.get('currentState') && projToJump.length === 1) {
                projToJump.find('.thumbContent').click();
            }
            else {
                projectsModel.projectToSet = modelId;
                projects.projectListView.loadProjects({
                    projState: modelState,
                    projYear: projectYear,
                    forceUpdate: true
                });
            }
        },

        //
        checkKeyup: function(event) {
            event.preventDefault();
            var keycode = event.keyCode || event.which;
            if(keycode === props.ENTER_KEY) {
                this.searchProject();
            }
        },

        //
        changeProjectsYear: function(event) {
            var currentYear = projectsModel.get('currentYear');
            if(event.target.id === 'arrowR' && (currentYear + 1 <= Util.getYear())) {
                projects.reloadPage({projYear: ++currentYear});
            }
            else if(event.target.id === 'arrowL') {
                projects.reloadPage({projYear: --currentYear});
            }
            else if($(event.target).is('input')) {
                $(event.target).select();
            }
        },

        //
        enterYear: function(event) {
            var century = String(Util.getYear()).slice(0,2),
                setYear = $(event.currentTarget).val();
            setYear = ~~(century + setYear.trim().slice(-2));
            $(event.currentTarget).blur();
            this.showCurrentYear();
            if(setYear > century && setYear <= Util.getYear()) {
                projects.reloadPage({projYear: setYear});
            }
        }
    });


    //
    function setEventHandlers() {
        //
        window.document.body.addEventListener('click', removeStatePointTool, true);

        //auto reload projects during inactivity
        window.document.body.addEventListener('mouseup', setTime);
        window.document.body.addEventListener('keyup', setTime);

        var refreshPeriod = 9 * 60 * 1000;   // 9 min
        function refresh() {
            var timeElapsed = new Date().getTime() - Projects.projectsTimer;
            if(timeElapsed >= (refreshPeriod - 100)) {   // 0.1s correction
                setTime();
                Projects.timerId = window.setTimeout(refresh, refreshPeriod);
                projects.reloadPage({forceUpdate: false});
            }
            else {
                Projects.timerId = window.setTimeout(refresh, refreshPeriod - timeElapsed);
            }
        }

        setTime();
        Projects.timerId = window.setTimeout(refresh, refreshPeriod);
    }

    //
    function removeStatePointTool(e) {
        if((!$(e.target).closest('#toggleStatePointTool').length && !$(e.target).closest('ul').is('.statePointBlock'))
            || $(e.target).is('.statePointDate')) {
            $('#toggleStatePointTool').removeClass('showTool');
            $('.statePoint').removeClass('selectedRow');
        }
    }

    //
    function setTime() {
        Projects.projectsTimer = new Date().getTime();
    }

    ///component API///

    //
    projects.showProjectDialog = function($elem, $content, title, saveCB, beforeCloseCB) {
        var buttons = {
            'Сохранить': saveCB
        };
        if(~title.indexOf('Редактировать проект')) {
            var projState = projectsModel.get('currentModel').get('projectState');
            if(projState === 'work') {
                buttons['В запросы'] = function() {
                    projects.projectListView.setProjectState('query');
                };
                buttons['Завершить проект'] = function() {
                    projects.projectListView.setProjectState('arch');
                }
            }
            else if(projState === 'arch' || projState === 'query') {
                buttons['Открыть проект'] = function() {
                    projects.projectListView.setProjectState('work');
                }
            }
        }
        Dialog.makeDialog({
            $elem: $elem,
            $content: $content,
            title: title,
            buttons: buttons,
            beforeClose: beforeCloseCB
        });
        var $projectText = $('#dialogContent').find('input[name=projectName]');
        if($.trim($projectText.val()) == '') {
            $projectText.focus();
        }
    };

    //
    projects.showTaskDialog = function($elem, $content, title, saveCB, beforeCloseCB) {
        var buttons = {},
            taskFinished = $elem.hasClass('taskFinished') || !!$content.find('textarea').attr('disabled');
        if(!taskFinished) {
            buttons['Сохранить'] = saveCB;
        }
        if(~title.indexOf('Редактировать задачу')) {
            buttons['Удалить задачу'] = function() {
                projects.taskCalendarView.deleteTask();
            }
        }
        if($elem.is('li.task')) {  //not new task
            if(taskFinished) {
                buttons['Открыть задачу'] = function() {
                    projects.taskCalendarView.changeTaskStatus('opened');
                };
            }
            else {
                buttons['Завершить задачу'] = function() {
                    projects.taskCalendarView.changeTaskStatus('finished');
                };
            }
        }
        Dialog.makeDialog({
            $elem: $elem,
            $content: $content,
            title: title,
            buttons: buttons,
            beforeClose: beforeCloseCB
        });
        var $taskText = $('#dialogContent').find('textarea[name=taskText]');
        if($.trim($taskText.val()) == '') {
            $taskText.focus();
        }
    };

    //
    projects.freeProject = function() {
        Util.requestJSON('projects/freeLock', {projectId: projectsModel.get('currentModel').get('id')});
    };

    //
    projects.loadComponent = function() {
        projectsModel = this.projectsModel = new ProjectsModel();
        props = this.props = {
            ENTER_KEY: 13,
            MIN_PROJECTNAME_LENGTH: 3,
            MAX_PROJECTNAME_LENGTH: 12,
            MAX_AVAILABLE_DATE: '9999-12-31',
            MAX_AVAILABLE_MONTH: '9999-12',
            PDF_FILE_MAXSIZE: 5,  //Mb (mySql max_allowed_packet)
            pointExecution: [
                [0, 2, 0, 3, 7, '3d-проектирование'],
                [1, 1, 1, 5, 9, 'детали: из листа'],  // pointStarted[0][1], pointToFinish[2][3], maxDaysForExecute[4], title[5]
                [1, 2, 1, 6, 11, 'детали: доски ДСП'],
                [1, 4, 1, 7, 11, 'детали: точеные'],
                [2, 7, 2, 9, 7, 'оплата заказа']
            ],
            stateCodesRus: {'work': 'рабочие', 'query': 'запросы', 'arch': 'архив', 'deleted': 'УДАЛЁН'}
        };
        props.linkRegexp = new RegExp('(^|[\\s.,:;!?_*()<>|\"\'\\[\\]])([#№][\\wа-яА-ЯЁё]{' + props.MIN_PROJECTNAME_LENGTH + ','
            + props.MAX_PROJECTNAME_LENGTH + '})(?=$|[\\s.,:;!?_*()<>|\"\'\\[\\]])', 'gi');

        this.projectsView = new ProjectsView({model: projectsModel});
        this.projectTools.loadComponent();
        this.projectList.loadComponent();
        this.taskCalendar.loadComponent();
    };

    //
    projects.unloadComponent = function() {
        window.document.body.removeEventListener('click', removeStatePointTool, true);
        window.document.body.removeEventListener('mouseup', setTime);
        window.document.body.removeEventListener('keyup', setTime);
        clearTimeout(this.timerId);
    };

    //
    projects.setProjectState = function(state, currentPassword) {
        this.projectListView.setProjectState(state, currentPassword);
    };

    //
    projects.showDeletingProject = function() {
        projectsModel.get('$currentThumb').addClass('deletingProject');
    };
    projects.cancelDeletingProject = function() {
        projectsModel.get('$currentThumb').removeClass('deletingProject');
    };

    //
    projects.renderProjectList = function() {
        this.projectListView.render();
    };

    //
    projects.renderCalendar = function() {
        this.taskCalendarView.render();
    };

    //
    projects.reloadPage = function(params) {
        params = params || {};
        if(typeof params.forceUpdate === 'undefined') {
            params.forceUpdate = true;  //'true' default for reloadPage
        }
        this.projectListView.loadProjects(params);
    };

    //
    projects.goToProject = function(proj) {
        var _this = this;
        Util.requestJSON('projects/searchProject', {searchNumber: proj.slice(1), searchType: 'projectCustomer'})
            .done(function(response) {
                if(response.status === 'OK' && response.data.length === 1) {
                    _this.projectsView.jumpToProject(response.data[0]);
                }
            });
    };

    //
    projects.makeProjectLinks = function(textString) {
        return textString.replace(props.linkRegexp, function(str, group1, group2) {
            return group1 + '<span class="extProjectLink">' + group2.toUpperCase() + '</span>';
        });
    };

    //
    projects.getAjaxRequestDescription = function(settings) {
        if(~settings.url.indexOf('projects/rest')) {
            var typeList = {
                'GET': 'загрузка проектов',
                'POST': 'добавление проекта',
                'PATCH': 'обновление проекта'
            };
            return typeList[settings.type];
        }
        else {
            var action = settings.url.slice(settings.url.indexOf('/') + 1),
                actionList = {
                    'checkLock': 'проверка блокировки',
                    'freeLock': 'снятие блокировки',
                    'deleteProject': 'удаление проекта',
                    'searchProject': 'поиск проекта',
                    'getProjectSpec': 'открытие спецификации',
                    'handleProjectSpec': 'сохранение спецификации'
                };
            return actionList[action];
        }
    };

    //
    projects.b64StringToArrayBuffer = function(b64String) {
        var str = window.atob(b64String.slice(b64String.indexOf(',') + 1)),
            buf = new ArrayBuffer(str.length),
            bufView = new Uint8Array(buf);
        for(var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    };


    return projects;

})(window, jQuery, Backbone, _);
