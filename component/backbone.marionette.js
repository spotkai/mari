(function(global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('backbone'), require('underscore'), require('backbone.radio')) :
        typeof define === 'function' && define.amd ? define(['backbone.marionette', 'underscore', 'backbone.radio'], factory) :
            (global.Marionette = global['Mn'] = factory(global.Backbone, global._, global.Backbone.Radio));
}(this, (function(Backbone, _, Radio) {
    'use strict';

    Backbone = 'default' in Backbone ? Backbone['default'] : Backbone;


// Ensure it can trigger events with Backbone.Events
    _.extend(MarionetteObject.prototype, Backbone.Events, CommonMixin, RadioMixin, {
        destroy: function destroy() {
            if(this._isDestroyed) {
                return this;
            }

            for(var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            this.triggerMethod.apply(this, ['before:destroy', this].concat(args));

            this._isDestroyed = true;
            this.triggerMethod.apply(this, ['destroy', this].concat(args));
            this.stopListening();

            return this;
        }
    });


    var ViewMixin = {

        // Handle destroying the view and its children.
        destroy: function destroy() {
            if(this._isDestroyed) {
                return this;
            }
            var shouldTriggerDetach = !!this._isAttached;

            for(var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            this.triggerMethod.apply(this, ['before:destroy', this].concat(args));
            if(shouldTriggerDetach) {
                this.triggerMethod('before:detach', this);
            }

            // unbind UI elements
            this.unbindUIElements();

            // remove the view from the DOM
            this.removeEl(this.el);

            if(shouldTriggerDetach) {
                this._isAttached = false;
                this.triggerMethod('detach', this);
            }

            // remove children after the remove to prevent extra paints
            this._removeChildren();

            this._destroyBehaviors(args);

            this._isDestroyed = true;
            this._isRendered = false;
            this.triggerMethod.apply(this, ['destroy', this].concat(args));

            this.stopListening();

            return this;
        }
    };


    function destroyBackboneView(view) {
        if(!view.supportsDestroyLifecycle) {
            triggerMethodOn(view, 'before:destroy', view);
        }

        var shouldTriggerDetach = !!view._isAttached;

        if(shouldTriggerDetach) {
            triggerMethodOn(view, 'before:detach', view);
        }

        view.remove();

        if(shouldTriggerDetach) {
            view._isAttached = false;
            triggerMethodOn(view, 'detach', view);
        }

        view._isDestroyed = true;

        if(!view.supportsDestroyLifecycle) {
            triggerMethodOn(view, 'destroy', view);
        }
    }


// Region
// ------
    var Region = MarionetteObject.extend({

        _empty: function _empty(view, shouldDestroy) {
            view.off('destroy', this._empty, this);
            this.triggerMethod('before:empty', this, view);

            this._restoreEl();

            delete this.currentView;

            if(!view._isDestroyed) {
                this._removeView(view, shouldDestroy);
                this._stopChildViewEvents(view);
            }

            this.triggerMethod('empty', this, view);
        },
        _stopChildViewEvents: function _stopChildViewEvents(view) {
            var parentView = this._parentView;

            if(!parentView) {
                return;
            }

            this._parentView.stopListening(view);
        },
        _removeView: function _removeView(view, shouldDestroy) {
            if(!shouldDestroy) {
                this._detachView(view);
                return;
            }

            if(view.destroy) {
                view.destroy();
            } else {
                destroyBackboneView(view);
            }
        },
        detachView: function detachView() {
            var view = this.currentView;

            if(!view) {
                return;
            }

            this._empty(view);

            return view;
        },
        _detachView: function _detachView(view) {
            var shouldTriggerDetach = !!view._isAttached;
            if(shouldTriggerDetach) {
                triggerMethodOn(view, 'before:detach', view);
            }

            this.detachHtml();

            if(shouldTriggerDetach) {
                view._isAttached = false;
                triggerMethodOn(view, 'detach', view);
            }
        },


        // Override this method to change how the region detaches current content
        detachHtml: function detachHtml() {
            this.detachContents(this.el);
        },


        // Reset the region by destroying any existing view and clearing out the cached `$el`.
        // The next time a view is shown via this region, the region will re-query the DOM for
        // the region's `el`.
        reset: function reset(options) {
            this.empty(options);

            if(this.$el) {
                this.el = this._initEl;
            }

            delete this.$el;
            return this;
        },
        destroy: function destroy(options) {
            this.reset(options);
            return MarionetteObject.prototype.destroy.apply(this, arguments);
        }
    });


    var RegionsMixin = {

        _removeRegion: function _removeRegion(region, name) {
            this.triggerMethod('before:remove:region', this, name, region);

            region.destroy();

            delete this.regions[name];
            delete this._regions[name];

            this.triggerMethod('remove:region', this, name, region);
        },

        detachChildView: function detachChildView(name) {
            return this.getRegion(name).detachView();
        },
        getChildView: function getChildView(name) {
            return this.getRegion(name).currentView;
        }
    };


// Container Methods
// -----------------

    _.extend(Container.prototype, {


        // To be used when avoiding call _updateLength
        // When you are done adding all your new views
        // call _updateLength
        _remove: function _remove(view) {
            var viewCid = view.cid;

            // delete model index
            if(view.model) {
                delete this._indexByModel[view.model.cid];
            }

            // delete custom index
            _.some(this._indexByCustom, _.bind(function(cid, key) {
                if(cid === viewCid) {
                    delete this._indexByCustom[key];
                    return true;
                }
            }, this));

            // remove the view from the container
            delete this._views[viewCid];

            return this;
        },


        // Update the `.length` attribute on this container
        _updateLength: function _updateLength() {
            this.length = _.size(this._views);

            return this;
        }
    });

// Collection View
// ---------------

    var CollectionView = Backbone.View.extend({


        // Handle a child added to the collection
        _onCollectionAdd: function _onCollectionAdd(child, collection, opts) {
            // `index` is present when adding with `at` since BB 1.2; indexOf fallback for < 1.2
            var index = opts.at !== undefined && (opts.index || collection.indexOf(child));

            // When filtered or when there is no initial index, calculate index.
            if(this.filter || index === false) {
                index = _.indexOf(this._filteredSortedModels(index), child);
            }

            if(this._shouldAddChild(child, index)) {
                this._destroyEmptyView();
                this._addChild(child, index);
            }
        },


        // Handle collection update model removals
        _onCollectionUpdate: function _onCollectionUpdate(collection, options) {
            var changes = options.changes;
            this._removeChildModels(changes.removed);
        },


        // Remove the child views and destroy them.
        // This function also updates the indices of later views
        // in the collection in order to keep the children in sync with the collection.
        // "models" is an array of models and the corresponding views
        // will be removed and destroyed from the CollectionView
        _removeChildModels: function _removeChildModels(models) {
            // Used to determine where to update the remaining
            // sibling view indices after these views are removed.
            var removedViews = this._getRemovedViews(models);

            if(!removedViews.length) {
                return;
            }

            this.children._updateLength();

            // decrement the index of views after this one
            this._updateIndices(removedViews, false);

            if(this.isEmpty()) {
                this._showEmptyView();
            }
        },

        _removeChildView: function _removeChildView(view) {
            this.triggerMethod('before:remove:child', this, view);

            this.children._remove(view);
            if(view.destroy) {
                view.destroy();
            } else {
                destroyBackboneView(view);
            }

            this.stopListening(view);
            this.triggerMethod('remove:child', this, view);
        },


        // Internal method to destroy an existing emptyView instance if one exists. Called when
        // a collection view has been rendered empty, and then a child is added to the collection.
        _destroyEmptyView: function _destroyEmptyView() {
            if(this._showingEmptyView) {
                this.triggerMethod('before:remove:empty', this);

                this._destroyChildren();
                delete this._showingEmptyView;

                this.triggerMethod('remove:empty', this);
            }
        },


        // Remove the child view and destroy it. This function also updates the indices of later views
        // in the collection in order to keep the children in sync with the collection.
        removeChildView: function removeChildView(view) {
            if(!view || view._isDestroyed) {
                return view;
            }

            this._removeChildView(view);
            this.children._updateLength();
            // decrement the index of views after this one
            this._updateIndices(view, false);
            return view;
        },


        // Destroy the child views that this collection view is holding on to, if any
        _destroyChildren: function _destroyChildren(options) {
            if(!this.children.length) {
                return;
            }

            this.triggerMethod('before:destroy:children', this);
            this.children.each(_.bind(this._removeChildView, this));
            this.children._updateLength();
            this.triggerMethod('destroy:children', this);
        }
    });


    var Behavior = MarionetteObject.extend({
        destroy: function destroy() {
            this.stopListening();

            return this;
        }
    });


    return Marionette;

})));
