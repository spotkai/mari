(function(window, $) {
    "use strict";

    var $body = $('body'),
        ddOpened = false;

    $body.on('click', '.dropdown, .dropup', clickDropdown);
    $body.on('mouseleave', '.dropdown, .dropup', leaveDropdown);
    $body.on('mouseover', '.dropdown, .dropup', overDropdown);

    //dropdown menus
    function clickDropdown(event) {
        var $target = $(event.currentTarget);
        event.preventDefault();
        if($target.find('ul.dropdownMenu').css('display') === 'none') {
            $target.addClass('open');
            ddOpened = true;
        }
        else {
            $target.removeClass('open');
            ddOpened = false;
        }
    }

    //disable dropdown menus
    function leaveDropdown() {
        var $ddMenus = $('.dropdown, .dropup');
        $ddMenus.removeClass('open');
        window.setTimeout(function() {
            if(!$ddMenus.hasClass('open')) {
                ddOpened = false;
            }
        }, 300);
    }

    //open next dropdown without click
    function overDropdown(event) {
        if(ddOpened) {
            $(event.currentTarget).addClass('open');
        }
    }

})(window, jQuery);