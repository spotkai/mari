var Monitor = (function(window, $, _) {
    "use strict";

    var monitor = {},        //instance
        monitorData = {execTimes: []};   //value stack
    //fixme add loadPageTime, parsePageTime
    //todo use performance.now()
    //todo use window.performance.memory.usedJSHeapSize
    //todo use window.performance.getEntriesByType("resource").length
    //todo see www.webpagetest.org

    ////
    //monitor.startMeasureTime = function() {
    //    monitorData.timeStart = new Date().getTime();
    //};
    //
    //
    ////
    //monitor.stopMeasureTime = function() {
    //    var executionTime = new Date().getTime() - monitorData.timeStart;
    //    monitorData.timeStart = null;
    //    monitorData.execTimes.push(executionTime);
    //    console.log(executionTime);  //todo remove
    //};
    //
    //
    ////$(window).unload
    //monitor.logExecutionTime = function(resource) {
    //    var minTime = _.min(monitorData.execTimes),
    //        maxTime = _.max(monitorData.execTimes),
    //        averageTime = _.reduce(monitorData.execTimes, function(memo, num) {
    //                return memo + num;
    //            }, 0) / (monitorData.execTimes.length === 0 ? 1 : monitorData.execTimes.length);
    //    monitorData.execTimes = [];
    //
    //    Util.requestJSON('monitor/logExecutionTime', {
    //        resource: resource,
    //        executionTime: {averageTime: averageTime, minTime: minTime, maxTime: maxTime}
    //    });
    //};


    //
    monitor.logJsError = function(message, source, error) {
        Util.requestJSON('monitor/logJsError', {
            message: message,
            source: source,
            stack: error.stack
        })
            .done(function(msg) {
                Util.stopLoadingAnimation();
            });
    };


    //
    monitor.getAjaxRequestDescription = function(settings) {
        var action = settings.url.slice(settings.url.indexOf('/') + 1),
            actionList = {
                'logExecutionTime': 'logExecutionTime',
                'logJsError': 'logJsError'
            };
        return actionList[action];
    };


    return monitor;
})(window, jQuery, _);
