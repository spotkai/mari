(function(window, $) {
    "use strict";

    $('body').on('click', '.accordionSection', clickAccordion);

    //accordion
    function clickAccordion(event) {
        var $target = $(event.target);
        if($target.is('.accordionToggler')) {
            if($target.closest('.accordion').is(':not(.current)')) {
                $(event.currentTarget).find('.accordion:not(.closed)').addClass('closed').removeClass('current');
                $target.closest('.accordion').removeClass('closed').addClass('current');
            }
            else {
                $target.closest('.accordion').addClass('closed').removeClass('current');
            }
        }
    }

})(window, jQuery);