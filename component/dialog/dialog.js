var Dialog = (function(window, $, Backbone, _) {
    "use strict";

    var dialog = {},  //instance
        dialogView;

    //
    var DialogView = Backbone.View.extend({
        tagName: 'div',
        id: 'dialogContainer',
        events: {
            'change #projectImg': 'loadProjectImg',
            'change #userAvatar': 'loadUserAvatar',
            'focus input, textarea': function() {
                $('#dialogMsg').empty();
            },
            'click .addInputField': 'addInputField',
            'click #dialogCloser': 'destroyDialog',
            'mousedown #dialogTitle': 'moveDialog'
        },

        initialize: function() {
            this.beforeClose = null;
        },

        render: function() {
            this.$el.html(templates['dialog']());
            return this;
        },


        //
        makeDialog: function(params) {
            $('#dialogShadow').addClass('fog');
            this.$('#dialogTitle').html(params.title);
            this.$('#dialogContent').html(params.$content);
            $.each(params.buttons, function(i, item) {
                var buttonClass = 'btn';
                if(~i.toLowerCase().indexOf('удалить')) {
                    buttonClass += ' btnError';
                }
                $('<button>', {
                    class: buttonClass,
                    type: 'button',
                    html: i,
                    click: item
                })
                    .appendTo('#dialogButtons');
            });
            this.beforeClose = params.beforeClose || null;

            var coords = this.getCoords(params.$elem),
                pickerLink = this.$('.datepickerLink');

            this.$el.css({top: coords.top, left: coords.left});
            if(pickerLink.length) {
                pickerLink.pickadate({
                    onOpen: function() {
                        var $pickerRoot = this.$root;
                        $pickerRoot.css('top', 'auto');
                        var pickerHeight = $pickerRoot.find('.picker__frame').outerHeight(true),
                            bottomPosition = $pickerRoot.offset().top + pickerHeight;
                        if(bottomPosition > $(window).height() - 12) {       //fixme 12
                            $pickerRoot.css('top', -pickerHeight);
                        }
                    }
                });
            }
            this.$el.addClass('scene');
            $('body').css('overflow-y', 'auto');
        },


        //
        destroyDialog: function() {
            if(!this.$el.hasClass('scene')) {   //not active
                return false;
            }
            if(typeof this.beforeClose === 'function') {
                this.beforeClose();
                this.beforeClose = null;
            }
            window.scrollTo(0, 0);
            $('body').css('overflow-y', 'hidden');
            this.$el.removeClass('scene');
            this.$('#dialogTitle').empty();
            this.$('#dialogContent').empty();
            this.$('#dialogMsg').empty();
            this.$('#dialogButtons').find('button').remove();
            $('#dialogShadow').removeClass('fog');
        },


        //
        getCoords: function($elem) {
            var elem = $elem[0];
            var gapY = 4, //px
                gapX = 4;

            var box = elem.getBoundingClientRect(),
                scrollTop = window.pageYOffset,
                scrollLeft = window.pageXOffset,
                elemHeight = $(elem).outerHeight(true),
                elemWidth = $(elem).outerWidth(true),
                dialogHeight = this.$el.outerHeight(true),
                dialogWidth = this.$el.outerWidth(true),
                windowHeight = $(window).height(),
                windowWidth = $(window).width();

            var top = box.top + scrollTop + gapY,
                left = box.left + scrollLeft - dialogWidth - gapX;

            //check that places in window at left
            if(left < 0) {
                top += elemHeight;
                left = box.left + scrollLeft;
            }

            //check that places in window at bottom
            if(top + dialogHeight > scrollTop + windowHeight) {
                top = scrollTop + windowHeight - dialogHeight - gapY;
                //move to right if covers elem
                if(left + dialogWidth > box.left + scrollLeft + gapX) {
                    left = box.left + scrollLeft + elemWidth + gapX;
                    //check place at right
                    if(left + dialogWidth > windowWidth + scrollLeft + gapX) {
                        left = scrollLeft + gapX;
                        top = box.top - dialogHeight + scrollTop - gapY;
                        if(box.left > 0) {
                            left += box.left;
                        }
                    }
                    //move up to elem
                    else if(box.top + scrollTop < top) {
                        top = box.top + scrollTop;
                    }
                }
            }   //set at top
            if(dialogHeight > windowHeight || top < 0) {
                top = gapY;
            }

            return {top: Math.round(top), left: Math.round(left)};
        },


        //
        showMsg: function(msg) {
            this.$('#dialogMsg').html(msg);
        },


        //
        loadProjectImg: function(event) {
            Util.loadImg(event, 120)
                .done(function(imageData) {
                    $(event.target).siblings('span.loadedImg')
                        .css('background-image', 'url(' + imageData + ')');
                });
        },


        //
        loadUserAvatar: function(event) {
            Util.loadImg(event, 36)
                .done(function(imageData) {
                    $(event.target).siblings('span.loadedImg')
                        .css('background-image', 'url(' + imageData + ')');
                });
        },


        //
        addInputField: function(event) {
            var $form = $(event.target).closest('form'),
                fullFieldNameRus = $(event.target).siblings('label').text(),
                fieldNameRus = fullFieldNameRus.slice(0, fullFieldNameRus.indexOf('-')),
                fullFieldName = $(event.target).siblings('input').attr('name'),
                fieldName = fullFieldName.slice(0, fullFieldName.indexOf('-')),
                nextFieldNumber = $form.find('input[name^=' + fieldName + ']').length + 1,
                $lastInput = $form.find('.formItem').has('input[name^=' + fieldName + ']').last();

            $(event.target).remove();
            $lastInput.after('<div class="formItem">\
                   <label for="' + fieldName + '-' + nextFieldNumber + '">' + fieldNameRus + '-' + nextFieldNumber + '</label>\
                   <input type="text" id="' + fieldName + '-' + nextFieldNumber + '" name="' + fieldName + '-' + nextFieldNumber + '">\
                   <span class="addInputField"></span></div>');
            $form.find('input[name="' + fieldName + '-' + nextFieldNumber + '"]').focus();
        },


        //
        moveDialog: function(ev) {
            if(ev.which !== 1) {  //not left button
                return false;
            }
            var _this = this,
                dialogWidth = this.$el.outerWidth(true),
                dialogHeight = this.$el.outerHeight(true),
                windowWidth = $(window).width(),
                windowHeight = $(window).height(),
                initX = this.$el.offset().left,
                initY = this.$el.offset().top,
                shiftX = ev.pageX - initX,
                shiftY = ev.pageY - initY;

            document.onmousemove = function(ev) {
                var moveX = ev.pageX - shiftX,
                    moveY = ev.pageY - shiftY;
                if(moveX > 0 && moveX < windowWidth - dialogWidth) {
                    _this.$el.css({left: moveX});
                }
                if(moveY > 0 && moveY < windowHeight - dialogHeight) {
                    _this.$el.css({top: moveY});
                }
            };

            document.onmouseup = function() {
                document.onmousemove = null;
                document.onmouseup = null;
            };
        }
    });


    //component API
    dialog.loadComponent = function() {
        dialogView = new DialogView();
        $('body').append('<div id="dialogShadow"></div>');
        return dialogView.render().el;
    };

    dialog.makeDialog = function(params) {
        dialogView.makeDialog(params);
    };

    dialog.destroyDialog = function() {
        dialogView.destroyDialog();
    };

    dialog.showMsg = function(msg) {
        dialogView.showMsg(msg);
    };

    return dialog;

})(window, jQuery, Backbone, _);
