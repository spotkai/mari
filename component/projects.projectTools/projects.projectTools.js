Projects.projectTools = (function(window, $, Backbone, _) {
    "use strict";

    var projectTools = {},    //instance
        projectsModel = {},   //common projectsModel
        props = {};           //common props


    //
    var ProjectToolsView = Backbone.View.extend({
        tagName: 'div',
        id: 'projectThumbTools',
        events: {
            'click #toolsLine span': function(ev) {
                var action = ev.currentTarget.id;
                this.thumbState[action] = !this.thumbState[action];
                $(ev.currentTarget).toggleClass('active');
                this[action + 'ToolAction'](ev, projectsModel.get('$currentThumb'));
                Projects.projectListView.scrollToThumb(600);
            }
        },

        initialize: function() {
            this.thumbState = {
                showAddTask: false,
                showEditProject: false,
                showTaskHistory: false,
                showDetails: false,
                showProjectSpec: false
            };
            this.render();
        },

        render: function() {
            this.$el.html(templates['projectTools']());
            return this;
        },

        //
        detachToolsPanel: function() {
            this.$el.removeClass('show').detach();
        },

        //
        _attachToolsPanel: function() {
            this._configToolsPanel();
            this.$el.appendTo(projectsModel.get('$currentThumb')).addClass('show');
        },

        //
        moveToolsPanel: function() {
            this.detachToolsPanel();
            this._attachToolsPanel();
            if(projectsModel.get('previousProjectId') !== projectsModel.get('currentModel').get('id')) {
                this.$el.css({left: -20}).animate({left: 0}, 200);
            }
            this._restoreThumbState();
        },

        //
        _configToolsPanel: function() {
            this.$('span').removeClass('hidden');
            if(projectsModel.get('currentState') === "arch") {
                this.$('#showAddTask').addClass('hidden');
            }
            if(!projectsModel.get('currentModel').get('projectSpecLoaded')) {
                this.$('#showProjectSpec').addClass('hidden');
            }
        },

        //
        _restoreThumbState: function() {
            var state,
                restoreStates = ['showTaskHistory', 'showDetails'];
            if(_.isFinite(projectsModel.get('previousProjectId'))) {
                //on select other thumb - close all in prev Model
                if(projectsModel.get('previousProjectId') !== projectsModel.get('currentModel').get('id')) {
                    var $previousProject = $('#project_' + projectsModel.get('previousProjectId'));
                    for(state in this.thumbState) {
                        if(this.thumbState.hasOwnProperty(state) && !!~_.indexOf(restoreStates, state) && this.thumbState[state]) {
                            this[state + 'ToolAction'](null, $previousProject);
                        }
                    }
                    this._clearThumbToolsState();
                }
                else {      //on reload - open all in same Model
                    for(state in this.thumbState) {
                        if(this.thumbState.hasOwnProperty(state) && !!~_.indexOf(restoreStates, state) && this.thumbState[state]) {
                            this[state + 'ToolAction'](null, projectsModel.get('$currentThumb'));
                        }
                    }
                    Projects.projectListView.scrollToThumb(0);
                }
            }
            else {
                this._clearThumbToolsState();
            }
        },

        //
        _clearThumbToolsState: function() {
            for(var state in this.thumbState) {
                if(this.thumbState.hasOwnProperty(state) && this.thumbState[state]) {
                    this.thumbState[state] = false;
                    this.$('span#' + state).removeClass('active');
                }
            }
        },

        //
        showAddTaskToolAction: function(ev) {     //fixme use (ev, $project)
            var _this = this,
                requestParams = {
                    projectId: projectsModel.get('currentModel').get('id'),
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime'),
                    userId: UserPanel.getUserModelData('id')
                };
            Util.requestJSON('projects/checkLock', requestParams)
                .done(function(message) {
                    if(message.status === 'OK') {
                        projectsModel.set({'currentTask': null}, {silent: true});
                        Projects.projectListView.setProjects(message.data);
                        Projects.showTaskDialog(projectsModel.get('$currentThumb'),
                            $(templates['addTask']()),
                            projectsModel.get('currentModel').get('projectName') + ': Добавить задачу',
                            function() {
                                Projects.taskCalendarView.addTask();
                            },
                            function() {
                                Projects.freeProject();
                                _this.thumbState[ev.currentTarget.id] = false;
                                $(ev.currentTarget).removeClass('active');
                            });
                    }
                    else {
                        _this.thumbState[ev.currentTarget.id] = false;
                        $(ev.currentTarget).removeClass('active');
                        if(message.status === 'ERROR_USER') {
                            Main.showNotice({
                                msg: 'Проект уже редактируется пользователем "'
                                + UserPanel.getUserModelData('allUsers')[message.data] + '".'
                            });
                        }
                        else if(message.status === 'ERROR') {
                            Main.showNotice({msg: message.data});
                        }
                    }
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    _this.thumbState[ev.currentTarget.id] = false;
                    $(ev.currentTarget).removeClass('active');
                });
        },

        //
        showEditProjectToolAction: function(ev) {      //fixme use (ev, $project)
            var _this = this,
                requestParams = {
                    projectId: projectsModel.get('currentModel').get('id'),
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime'),
                    userId: UserPanel.getUserModelData('id')
                };
            Util.requestJSON('projects/checkLock', requestParams)
                .done(function(message) {
                    if(message.status === 'OK') {
                        Projects.projectListView.setProjects(message.data);
                        Projects.showProjectDialog(projectsModel.get('$currentThumb'),
                            $(templates['addProject'](projectsModel.get('currentModel').attributes)),
                            projectsModel.get('currentModel').get('projectName') + ': Редактировать проект',
                            function() {
                                Projects.projectListView.editProject();
                            },
                            function() {
                                Projects.freeProject();
                                _this.thumbState[ev.currentTarget.id] = false;
                                $(ev.currentTarget).removeClass('active');
                            });
                    }
                    else {
                        _this.thumbState[ev.currentTarget.id] = false;
                        $(ev.currentTarget).removeClass('active');
                        if(message.status === 'ERROR_USER') {
                            Main.showNotice({
                                msg: 'Проект уже редактируется пользователем "'
                                + UserPanel.getUserModelData('allUsers')[message.data] + '".'
                            });
                        }
                        else if(message.status === 'ERROR') {
                            Main.showNotice({msg: message.data});
                        }
                    }
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    _this.thumbState[ev.currentTarget.id] = false;
                    $(ev.currentTarget).removeClass('active');
                });
        },

        //
        showTaskHistoryToolAction: function(event, $project) {
            $project.find('ul.taskBlock li.taskFinished').toggleClass('scene');
        },

        //
        showDetailsToolAction: function(event, $project) {
            if($project.closest('#state-design').length) {
                $project.find('.projectDetails').css('right', -298).toggleClass('detOpened');
            }
            else {
                $project.find('.projectDetails').css('left', -298).toggleClass('detOpened');
            }
        },

        //
        showProjectSpecToolAction: function(ev) {        //fixme use (ev, $project)
            var _this = this;
            _this.thumbState[ev.currentTarget.id] = false;
            $(ev.currentTarget).removeClass('active');
            Projects.projectListView.showFileView();

        }
        //fixme move all not same func To projects (use this.$)

    });


    //component API
    projectTools.loadComponent = function() {
        projectsModel = Projects.projectsModel;
        props = Projects.props;
        Projects.projectToolsView = new ProjectToolsView();
    };


    return projectTools;

})(window, jQuery, Backbone, _);