Projects.taskCalendar = (function(window, $, Backbone, _) {
    "use strict";

    var calendar = {},         //instance
        projectsModel = {},    //common projectsModel
        props = {};            //common props


    //
    var TaskCalendarView = Backbone.View.extend({
        el: '#taskCalendar',
        events: {
            'click li.task': 'selectTask',
            'click span.showEditTask': 'changeTask',
            'click .extProjectLink': 'projectJumping',
            'change #taskPerformerFilter': function(ev) {
                projectsModel.set('performerFilter', $(ev.target).val());
            }
        },

        initialize: function() {
            projectsModel.set({'performerFilter': UserPanel.getUserModelData('id').toString()}, {silent: true});
            this.$el.prepend(templates['taskListFilter']);
            this.tasks = [];
            this.tasksCount = 0;
            this.listenTo(projectsModel, 'change:currentTask', this.showSelectedTask);
            this.listenTo(projectsModel, 'change:performerFilter', this.render);
        },

        //
        render: function() {
            this.generateTaskList(projectsModel.get('performerFilter'));
            this.$('#taskCount').html(this.tasksCount);
            this.$('#taskCalendarContent').html(templates['taskList']({taskList: this.tasks}));
            if(projectsModel.get('currentTask')) {
                projectsModel.trigger('change:currentTask');
            }
        },

        //
        selectTask: function(ev) {
            if($(ev.target).is('.showEditTask')) {
                return false;
            }
            var $currentTask = $(ev.currentTarget).attr('data-id'),
                currentModelId = $currentTask.slice(0, $currentTask.indexOf('_')),
                currentTaskId = $currentTask.slice($currentTask.indexOf('_') + 1) || null;
            $('#project_' + currentModelId).find('.thumbContent').click();
            projectsModel.set('currentTask', {
                'currentModelId': currentModelId,
                'currentTaskId': currentTaskId
            });
        },

        //
        showSelectedTask: function() {
            this.$('li.task').removeClass('selectedTask')
                .find('.showEditTask').remove();
            if(projectsModel.get('currentTask')) {   //when selected thumb changes
                var dataId = projectsModel.get('currentTask').currentModelId + '_' + projectsModel.get('currentTask').currentTaskId,
                    $task = this.$('li.task[data-id=' + dataId + ']');
                if($task.length) {  //for finished tasks
                    $task.addClass('selectedTask').append('<span class="showEditTask hoverCircle"></span>');
                    this.scrollToTask($task);
                }
            }
        },

        //
        scrollToTask: function($task) {
            var scrollTop = this.$('#taskCalendarContent').scrollTop(),
                calendarPos = this.$('#taskCalendarContent').offset().top + this.$('#taskCalendarContent').outerHeight(true),
                filterPos = this.$('#taskListFilter').offset().top + this.$('#taskListFilter').outerHeight(true),
                topPosition = $task.offset().top,
                bottomPosition = topPosition + $task.outerHeight(true),
                gapY = 26, gapY2 = 5;

            if(topPosition < filterPos + gapY) {
                this.$('#taskCalendarContent').animate({'scrollTop': (scrollTop + topPosition - filterPos - gapY)}, 600);
            }
            else if(bottomPosition > calendarPos - gapY2) {
                this.$('#taskCalendarContent').animate({'scrollTop': (scrollTop + bottomPosition - calendarPos + gapY2)}, 600);
            }
        },

        //
        addTask: function(status) {      //fixme add check something changed
            var formData = {};
            $('#formAddTask').find('textarea, input, select').each(function() {
                var value = null;
                if($(this).attr('name') === 'taskHighPriority') {
                    value = (status === 'finished') ? false : $(this).is(':checked');
                }
                else {
                    value = $(this).val() || null;
                }
                formData[$(this).attr('name')] = value;
            });
            if(formData['taskText']) {
                var tasks = projectsModel.get('currentModel').get('projectTasks');
                if(projectsModel.get('currentTask')) {
                    var taskId = +projectsModel.get('currentTask').currentTaskId;
                }
                else {     //new task
                    var taskId = this.calcNextTaskId(tasks);
                }
                tasks.push({
                    taskText: formData['taskText'], taskDate: formData['taskDate'],
                    taskPerformer: formData['taskPerformer'], taskStatus: status || 'opened',
                    taskHighPriority: formData['taskHighPriority'], taskId: taskId
                });
                tasks = this.sortTasks(tasks);
                this.saveTasks(tasks);
            }
            else {
                Dialog.showMsg('Введите текст задачи.');
            }
        },

        //
        changeTask: function(source) {
            var _this = this,
                requestParams = {
                    projectId: projectsModel.get('currentModel').get('id'),
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime'),
                    userId: UserPanel.getUserModelData('id')
                };

            Util.requestJSON('projects/checkLock', requestParams)
                .done(function(message) {
                    if(message.status === 'OK') {
                        Projects.projectListView.setProjects(message.data);

                        if('fromProject' === source) {
                            var $taskElement = projectsModel.get('$currentThumb')
                                .find('li.task[data-id=' + projectsModel.get('currentTask').currentTaskId + ']');
                        }
                        else {
                            var dataId = projectsModel.get('currentTask').currentModelId + '_' + projectsModel.get('currentTask').currentTaskId,
                                $taskElement = _this.$('li.task[data-id=' + dataId + ']');
                        }
                        var tasksNew = projectsModel.get('currentModel').get('projectTasks'),
                            taskIndexNew = _this.getTaskIndex();

                        if(tasksNew[taskIndexNew]) {
                            Projects.showTaskDialog($taskElement,
                                $(templates['addTask'](tasksNew[taskIndexNew])),
                                projectsModel.get('currentModel').get('projectName') + ': Редактировать задачу',
                                function() {
                                    tasksNew.splice(taskIndexNew, 1);
                                    _this.addTask();
                                },
                                function() {
                                    Projects.freeProject();
                                });
                        }
                        else {
                            Projects.freeProject();
                            Main.showNotice({msg: 'Задача была удалена другим пользователем.<br/>Обновите проекты.'});
                        }
                    }
                    else if(message.status === 'ERROR_USER') {
                        Main.showNotice({
                            msg: 'Проект уже редактируется пользователем "' + UserPanel.getUserModelData('allUsers')[message.data] + '".'
                        });
                    }
                    else if(message.status === 'ERROR') {
                        Main.showNotice({msg: message.data});
                    }
                });
        },

        //
        deleteTask: function() {
            var taskIndex = this.getTaskIndex(),
                tasks = projectsModel.get('currentModel').get('projectTasks');
            tasks.splice(taskIndex, 1);
            this.saveTasks(tasks);
        },

        //
        changeTaskStatus: function(status) {
            var taskIndex = this.getTaskIndex(),
                tasks = projectsModel.get('currentModel').get('projectTasks');
            if(status === 'finished') {
                tasks.splice(taskIndex, 1);
                this.addTask(status);
            }
            else {
                tasks[taskIndex].taskStatus = status;
                this.saveTasks(tasks);
            }
        },

        //
        saveTasks: function(tasks) {
            var requestParams = {
                state: projectsModel.get('currentState'),
                year: projectsModel.get('currentYear'),
                collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
            };
            projectsModel.get('currentModel').save(_.extend({projectTasks: tasks}, requestParams), {
                patch: true,
                wait: true,
                success: function(model, response, options) {
                    Dialog.destroyDialog();
                    if(response.status === 'OK') {
                        Projects.projectListView.setProjects(response.data);
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                }
            });
        },

        //
        generateTaskList: function(performer) {
            var j = 0,
                projTasks = null,
                taskList = [];
            Projects.projectListView.collection.forEach(function(project) {
                projTasks = project.get('projectTasks');
                projTasks.forEach(function(task) {
                    if(task.taskStatus === 'opened' && (performer === 'all' || performer === task.taskPerformer)) {
                        taskList[j] = {};
                        taskList[j].order = project.get('id') + '_' + task.taskId;
                        taskList[j].taskText = task.taskText;
                        taskList[j].taskDate = task.taskDate || '...';
                        taskList[j].taskPerformer = task.taskPerformer;
                        taskList[j].taskStatus = task.taskStatus;
                        taskList[j].taskHighPriority = task.taskHighPriority;
                        taskList[j].projectClient = project.get('projectClient') + " &#91;" + project.get('projectName') + "&#93;";
                        j++;
                    }
                });
            });

            this.tasksCount = taskList.length;
            taskList = this.sortTasks(taskList);
            taskList = _.groupBy(taskList, function(item) {
                return item.taskDate;
            });
            this.tasks = taskList;
        },

        //
        sortTasks: function(taskList) {
            return _.sortBy(taskList, function(item) {
                if(item.taskDate === '...') {
                    return props.MAX_AVAILABLE_DATE;
                }
                else {
                    if(item.taskHighPriority) {
                        return item.taskDate + '-0';
                    }
                    return item.taskDate + '-1';
                }
            });
        },

        //
        calcNextTaskId: function(tasks) {
            if(!tasks.length) {
                return 0;
            }
            return _.max(tasks, function(task) {
                    return task.taskId;
                }).taskId + 1;
        },

        //
        getTaskIndex: function() {
            var tasks = projectsModel.get('currentModel').get('projectTasks'),
                taskId = +projectsModel.get('currentTask').currentTaskId,
                taskIndex = -1;
            for(var i = 0, len = tasks.length; i < len; i++) {
                if(tasks[i].taskId === taskId) {
                    taskIndex = i;
                    break;
                }
            }
            return taskIndex;
        },

        //
        projectJumping: function(ev) {
            if($(ev.target).closest('.task').is('.selectedTask')) {
                ev.stopPropagation();
                Projects.goToProject(ev.target.innerText);
            }
        }
    });


    //component API
    calendar.loadComponent = function() {
        projectsModel = Projects.projectsModel;
        props = Projects.props;
        Projects.taskCalendarView = new TaskCalendarView();
    };


    return calendar;

})(window, jQuery, Backbone, _);