Projects.projectList = (function(window, $, Backbone, _) {
    "use strict";

    var projectList = {},     //instance
        projectsModel = {},   //common projectsModel
        props = {};           //common props


    //
    var Project = Backbone.Model.extend({
        defaults: {
            projectName: null,
            projectState: null,
            projectParams: null,
            projectImg: null,
            projectTasks: null,
            projectData: null,
            projectDateStart: null,
            projectDateFinish: null
        },

        initialize: function() {
            this.on('error', function(model, xhr, options) {
                Main.syncError(model, xhr, options);
            });
        },

        urlRoot: 'projects/rest'
    });


    //
    var ProjectView = Backbone.View.extend({
        tagName: 'section',
        className: 'projectThumb',
        events: {
            'click .thumbContent': 'setCurrentModel',
            'click .extProjectLink': 'projectJumping'
        },

        initialize: function() {
            this.listenTo(this.model, 'closeView', this.closeView);
        },

        render: function() {
            this.$el.html(templates['projectThumb'](this.model.attributes))
                .attr('id', 'project_' + this.model.get('id'));
            if(projectsModel.get('currentState') === 'work') {
                this.checkPointExecPeriod();  //fixme move here checking expired date
            }
            this.$el.addClass('thumb-' + projectsModel.get('currentState'));
            return this;
        },

        setCurrentModel: function() {
            if(this.model !== projectsModel.get('currentModel')) {
                projectsModel.set('previousProjectId', projectsModel.get('currentModel').get('id'));
                projectsModel.set('currentModel', this.model);
                projectsModel.set('currentTask', null);
            }
        },

        checkPointExecPeriod: function() {
            var _this = this,
                projData = this.model.get('projectData'),
                now = new Date().getTime();
            props.pointExecution.forEach(function(point) {    //fixme add generate systemTaskBlock msgs
                var pointStarted = projData[point[0]][point[1]],
                    pointToFinish = projData[point[2]][point[3]];

                if(pointToFinish && pointToFinish.status === 'inwork'
                    && pointStarted.date && now > new Date(pointStarted.date).getTime() + point[4] * 24 * 3600000) {
                    var $statePoint = _this.$el.find('.stateGroup[data-id=' + point[2] + '] .statePoint[data-id=' + point[3] + ']');
                    $statePoint.addClass('expired');
                }
            });
        },

        //
        projectJumping: function(ev) {
            if($(ev.target).closest('.projectThumb').is('.selected')) {
                var linkParent = $(ev.target).closest('.task');
                if(linkParent.length && !linkParent.is('.selectedTask')) {
                    return;
                }
                ev.stopPropagation();
                Projects.goToProject(ev.target.innerText);
            }
        }
    });


    //
    var ProjectList = Backbone.Collection.extend({
        model: Project,
        url: 'projects/rest',

        parse: function(response) {
            return response.data.collection;
        }
    });


    //
    var ProjectListView = Backbone.View.extend({
        el: '#projectList',
        events: {
            'click li.task': 'markTask',
            'click span.showEditTask': 'editTask',
            'click .statePoint': 'showToggleStatePointTool',
            'click #toggleStatePointTool': 'toggleStatePoint'
        },

        initialize: function() {
            this.toggleStateLink = null;
            this.collection = new ProjectList();
            this.viewCollection = [];
            this.listenTo(this.collection, 'error', function(collection, xhr, options) {
                Main.syncError(collection, xhr, options);
            });
            this.listenTo(projectsModel, 'change:currentModel', this.showSelectedProject);
            this.listenTo(projectsModel, 'change:currentModel', this.showFooterInfo);
            this.listenTo(projectsModel, 'change:currentTask', this.showMarkedTask);
            this.listenTo(projectsModel, 'change:performerFilter', this.render);
            this.loadProjects({forceUpdate: true});
        },

        //
        loadProjects: function(params) {
            params = params || {};
            var _this = this,
                projectState = params.projState || projectsModel.get('currentState'),
                projectYear = params.projYear || projectsModel.get('currentYear');
            if(params.projState && params.projState !== projectsModel.get('currentState') && !params.projYear) {   //projState changes
                projectYear = Util.getYear();   //set last year
            }

            var collectionType = {name: 'projects', projectState: projectState, projectYear: projectYear},
                updateParams = {};
            if(params.forceUpdate || !_.isEqual(JSON.parse(localStorage.getItem('collectionType')), collectionType)) {
                updateParams = {collectionForceUpdate: true};
            }
            else {
                updateParams = {collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')};
            }

            var requestParams = _.extend({state: projectState, year: projectYear}, updateParams);
            this.collection.fetch({
                data: $.param(requestParams),
                reset: !!requestParams['collectionForceUpdate'],
                success: function(collection, response, options) {
                    if(response.status === 'OK') {
                        projectsModel.set('currentState', projectState);
                        projectsModel.set('currentYear', projectYear);
                        localStorage.setItem('collectionType', JSON.stringify(collectionType));
                        localStorage.setItem('collectionUpdatedTime', response.data.collectionUpdatedTime);

                        _this.render();
                        Projects.renderCalendar();
                    }
                    else if(response.status === 'ERROR') {
                        // Main.showNotice({msg: response.data});
                    }
                }
            });
        },

        //
        setProjects: function(data) {
            this.collection.set(data.collection);
            localStorage.setItem('collectionUpdatedTime', data.collectionUpdatedTime);
            this.render();
            Projects.renderCalendar();
        },

        //
        render: function() {
            Projects.projectToolsView.detachToolsPanel();   //save toolsPanel before re-render projects
            this.viewCollection.forEach(function(item) {
                item.closeView();
            });
            this.viewCollection.length = 0;
            this.$('#projectListContent').html('');

            if(!this.collection.models.length) {      //empty
                projectsModel.set({'currentModel': null, '$currentThumb': null}, {silent: true});
                return false;
            }

            //render
            this.sortModels();
            var _this = this,
                states = {};
            states['state-design'] = document.createElement('div');
            states['state-production'] = document.createElement('div');
            states['state-shipping'] = document.createElement('div');
            states['state-design'].id = 'state-design';
            states['state-production'].id = 'state-production';
            states['state-shipping'].id = 'state-shipping';

            //accordion for arch & query projects
            if(projectsModel.get('currentState') === 'arch' || projectsModel.get('currentState') === 'query') {
                var date, projState, groupedModels,
                    accordionSection = document.createElement('ul');
                accordionSection.className = 'accordionSection';

                if(projectsModel.get('currentState') === 'arch') {
                    projState = 'shipping';
                    groupedModels = this.collection.groupBy(function(item) {
                        if(!item.get('projectDateFinish')) {
                            date = props.MAX_AVAILABLE_DATE;
                        }
                        else {
                            date = item.get('projectDateFinish');
                        }
                        return date.slice(0, 7);
                    });
                }
                else if(projectsModel.get('currentState') === 'query') {
                    projState = 'design';
                    groupedModels = this.collection.groupBy(function(item) {
                        return item.get('projectDateStart').slice(0, 7);
                    });
                }

                for(var finishDate in groupedModels) {
                    if(groupedModels.hasOwnProperty(finishDate)) {
                        var accordion = document.createElement('li');
                        accordion.className = 'accordion closed';

                        var accordionToggler = document.createElement('span');
                        accordionToggler.className = 'accordionToggler';
                        if(finishDate === props.MAX_AVAILABLE_MONTH) {
                            accordionToggler.textContent = 'без даты [' + groupedModels[finishDate].length + ']';
                        }
                        else {
                            accordionToggler.textContent = finishDate.slice(0, 4) + ' ' + Util.getMonthName(finishDate.slice(5, 7))
                                + ' [' + groupedModels[finishDate].length + ']';
                        }

                        var accordionContent = document.createElement('ul');
                        accordionContent.className = 'accordionContent';
                        groupedModels[finishDate].forEach(function(item) {
                            var projectView = new ProjectView({model: item});
                            var li = document.createElement('li');
                            li.appendChild(projectView.render().el);
                            accordionContent.appendChild(li);
                            _this.viewCollection.push(projectView);
                        });

                        accordion.appendChild(accordionToggler);
                        accordion.appendChild(accordionContent);
                        accordionSection.appendChild(accordion);
                    }
                }
                states['state-' + projState].appendChild(accordionSection);
            }

            else {
                this.collection.forEach(function(item) {
                    var projectView = new ProjectView({model: item}),
                        projState = _this.calcProjectState(item);
                    states['state-' + projState].appendChild(projectView.render().el);
                    _this.viewCollection.push(projectView);
                });
            }

            var fragment = document.createDocumentFragment();
            fragment.appendChild(states['state-design']);
            fragment.appendChild(states['state-production']);
            fragment.appendChild(states['state-shipping']);
            this.$('#projectListContent').append(fragment);
            states = null;

            this.setCurrentState();
            return this;
        },

        //
        sortModels: function() {
            //comparator
            this.collection.models = this.collection.sortBy(function(item) {
                if(projectsModel.get('currentState') === 'query') {
                    return -new Date(item.get('projectDateStart')).getTime();
                }
                else if(projectsModel.get('currentState') === 'arch') {
                    return -new Date(item.get('projectDateFinish')).getTime();
                }
                else {
                    return item.get('projectDateFinish') || props.MAX_AVAILABLE_DATE;
                }
            });
        },

        //
        setCurrentState: function() {
            //project has to set
            if(projectsModel.projectToSet) {
                projectsModel.set({'currentTask': null}, {silent: true});

                //set new added project as current
                var lastModel = this.collection.max(function(model) {
                    return model.get('id');
                });
                if(projectsModel.projectToSet === lastModel.get('projectName')) {
                    projectsModel.set('previousProjectId', null);
                    projectsModel.set({'currentModel': lastModel});
                }
                //use specified id
                else if(_.isFinite(projectsModel.projectToSet)) {
                    projectsModel.set('previousProjectId', projectsModel.get('currentModel').get('id'));
                    projectsModel.set({'currentModel': this.collection.get(projectsModel.projectToSet)});
                }
                projectsModel.projectToSet = null;
            }
            //if no currentModel(@init || previous currentModel has switched to other set<>)
            else if(!projectsModel.get('currentModel') || !this.collection.get(projectsModel.get('currentModel').get('id'))) {
                //set first as current
                projectsModel.set('previousProjectId', null);
                projectsModel.set({'currentTask': null}, {silent: true});
                projectsModel.set({'currentModel': this.collection.models[0]});
            }
            else {
                //set previous as current
                projectsModel.set({'currentModel': this.collection.get(projectsModel.get('currentModel').get('id'))}, {silent: true});
                projectsModel.set('previousProjectId', projectsModel.get('currentModel').get('id'));
                projectsModel.trigger('change:currentModel');  //silent & trigger for firing :change on same model (for showSelectedProject())
            }
        },

        //
        showSelectedProject: function() {
            var $project = $('#project_' + projectsModel.get('currentModel').get('id'));
            projectsModel.set('$currentThumb', $project);
            this.$('.selected, .expanded').removeClass('selected expanded');
            $project.addClass('selected');
            if(projectsModel.get('currentState') === 'query' || projectsModel.get('currentState') === 'arch') {
                $project.toggleClass('expanded');
                if($project.closest('.accordion').is(':not(.current)')) {
                    $project.closest('.accordion').find('.accordionToggler').click();
                }
            }

            this.scrollToThumb(600);
            Projects.projectToolsView.moveToolsPanel();
        },

        //
        scrollToThumb: function(duration) {
            var scrollTop = this.$el.scrollTop(),
                projectListPos = this.$el.offset().top + this.$el.outerHeight(true),
                headerPos = $('#projectsHeader').offset().top + $('#projectsHeader').outerHeight(true),
                topPosition = projectsModel.get('$currentThumb').offset().top,
                bottomPosition = topPosition + projectsModel.get('$currentThumb').outerHeight(true),
                $projectDetails = projectsModel.get('$currentThumb').find('.projectDetails'),
                gapY = 8;

            if($projectDetails.hasClass('detOpened')) {
                bottomPosition = topPosition + $projectDetails.outerHeight(true);
            }

            if(topPosition < headerPos + gapY) {
                this.$el.animate({'scrollTop': (scrollTop + topPosition - headerPos - gapY)}, duration);
            }
            else if(bottomPosition > projectListPos) {
                this.$el.animate({'scrollTop': (scrollTop + bottomPosition - projectListPos)}, duration);
            }
        },

        //
        showFooterInfo: function() {
            $('#projectFooter').find('span').html('');
            if(projectsModel.get('currentState') === 'work') {
                $('#projectFooter').find('span')
                    .html(projectsModel.get('currentModel').get('firstInworkPoint') || 'ЗАВЕРШЕНО');
            }
        },

        //
        addProject: function() {
            var _this = this,
                formData = this.getFormData('addProject'),
                requestParams = {
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
                };

            if(formData) {
                (new Project(formData)).save(requestParams, {
                    wait: true,
                    success: function(model, response, options) {
                        Dialog.destroyDialog();
                        if(response.status === 'OK') {
                            projectsModel.projectToSet = formData.projectName.toString();
                            _this.setProjects(response.data);
                        }
                        else if(response.status === 'ERROR') {
                            Main.showNotice({msg: response.data});
                        }
                    }
                });
            }
        },

        //
        editProject: function() {
            var _this = this,
                formData = this.getFormData('editProject'),
                requestParams = {
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
                };
            _.extend(formData, requestParams);

            if(formData) {
                projectsModel.get('currentModel').save(formData, {
                    patch: true,
                    wait: true,
                    success: function(model, response, options) {
                        Dialog.destroyDialog();
                        if(response.status === 'OK') {
                            _this.setProjects(response.data);
                        }
                        else if(response.status === 'ERROR') {
                            Main.showNotice({msg: response.data});
                        }
                    }
                });
            }
        },

        //
        getFormData: function(action) {
            var formData = {},
                data = {};
            $('#formAddProject').find('input, textarea').each(function() {
                var value = null;
                if($(this).attr('name') === 'projectImg') {
                    value = $(this).siblings('span.loadedImg')
                        .css('background-image').replace('url(', '').replace(')', '').replace(/^"(.*)"$/, '$1');
                }
                else if($(this).attr('name') === 'projectName') {
                    value = $(this).val().trim().toUpperCase().slice(0, props.MAX_PROJECTNAME_LENGTH) || null;
                }
                else if($(this).is('input')) {
                    value = $(this).val().replace(/"/g, '\'') || null;
                }
                else {
                    value = $(this).val() || null;
                }
                formData[$(this).attr('name')] = value;
            });

            data.projectImg = formData.projectImg;
            data.projectState = projectsModel.get('currentState');
            data.projectName = formData.projectName;
            data.projectDateFinish = formData.projectDateFinish;
            data.projectParams = [
                {title: 'код изделия', value: formData['paramSubjectCode']},
                {title: 'цвет металла', value: formData['paramColorMe']},
                {title: 'текстура ДСП', value: formData['paramTextureDSP']},
                {title: 'цвет кромки', value: formData['paramEdgeDSP']},
                {title: 'заметка', value: formData['paramNote']}
            ];

            return this.validateProjectFormData(data, action);
        },

        //
        validateProjectFormData: function(formData, action) {
            var errorMsg = null,
                names = this.collection.pluck('projectName');
            if(action !== 'addProject') {
                names = _.without(names, projectsModel.get('currentModel').get('projectName'));
            }

            if(!formData.projectName || formData.projectName.length < props.MIN_PROJECTNAME_LENGTH) {
                errorMsg = 'Укажите название проекта (min ' + props.MIN_PROJECTNAME_LENGTH + ' символов).';
            }
            else if(_.contains(names, formData.projectName)) {
                errorMsg = 'Такой проект уже существует.';
            }
            else if(action === 'editProject') {
                var modelIsChanged = false;
                for(var attr in formData) {
                    if(formData.hasOwnProperty(attr) && !_.isEqual(formData[attr], projectsModel.get('currentModel').get(attr))) {
                        modelIsChanged = true;
                        break;
                    }
                }
                if(!modelIsChanged) {
                    errorMsg = 'Ничего не изменено.';
                }
            }

            if(errorMsg) {
                Dialog.showMsg(errorMsg);
                return false;
            }
            return formData;
        },

        //
        markTask: function(event) {
            if($(event.target).is('.showEditTask')) {
                return false;
            }
            var currentTaskId = $(event.currentTarget).attr('data-id');
            projectsModel.set('currentTask', {
                'currentModelId': projectsModel.get('currentModel').get('id'),
                'currentTaskId': currentTaskId
            });
        },

        //
        showMarkedTask: function() {
            this.$('li.task').removeClass('selectedTask')
                .find('.showEditTask').remove();
            if(projectsModel.get('currentTask')) {   //when selected thumb changes
                $('#project_' + projectsModel.get('currentTask').currentModelId)
                    .find('li.task[data-id=' + projectsModel.get('currentTask').currentTaskId + ']').addClass('selectedTask')
                    .append('<span class="showEditTask hoverCircle"></span>');
            }
        },

        //
        editTask: function() {
            Projects.taskCalendarView.changeTask('fromProject');
        },

        //
        showToggleStatePointTool: function(ev) {
            if(this.$('.picker').length) {
                return;
            }
            var $project = $(ev.currentTarget).closest('.projectThumb'),
                $projectDetails = $(ev.currentTarget).closest('.projectDetails'),
                clickX = $(ev.currentTarget).position().left,
                clickY = $(ev.currentTarget).position().top,
                posX = clickX + $project.position().left + $projectDetails.position().left,
                posY = clickY + $project.position().top + $projectDetails.position().top + this.$el.scrollTop();
            this.toggleStateLink = $(ev.currentTarget);

            if($(ev.target).is('.statePointDate.datepickerLink')) {
                var $pickadateContainer = $('#pickadateContainer');
                posX += 38;
                posY += $(ev.currentTarget).outerHeight(true);
                $pickadateContainer.css({top: posY, left: posX}).addClass('scene');
                $(ev.target).pickadate({
                    container: $pickadateContainer,
                    onClose: function() {
                        $pickadateContainer.removeClass('scene');
                        Projects.projectListView.toggleStatePoint(ev);
                        this.stop();
                    }
                });
                return;
            }

            var $toggleStatePointTool = $('#toggleStatePointTool');
            $toggleStatePointTool.find('.formItem').css('display', 'none');
            $toggleStatePointTool.find('#addStatePointNote').css('display', 'block');
            $toggleStatePointTool.find('#statePointNote').val('');
            this.$('.statePoint').removeClass('selectedRow');

            posX += 20;
            if($(ev.currentTarget).closest('.stateGroup').is('[data-id=0]')) {
                posY += $(ev.currentTarget).outerHeight(true);
            }
            else {
                posY -= $toggleStatePointTool.outerHeight(true);
            }
            $toggleStatePointTool.css({top: posY, left: posX}).addClass('showTool');
            $(ev.currentTarget).addClass('selectedRow');
        },

        //
        toggleStatePoint: function(event) {
            var $currentTarget = this.toggleStateLink,
                pointName = $currentTarget.attr('data-id'),
                groupName = $currentTarget.closest('li.stateGroup').attr('data-id'),
                data = projectsModel.get('currentModel').get('projectData'),
                point = data[groupName][pointName];

            if($(event.target).is('.statePointDate.datepickerLink') && point.date !== $(event.target).val()) {
                if('date' in point) {
                    point.date = $(event.target).val();
                }
                this.saveProjectData({groupName: groupName, pointName: pointName, point: point});
            }

            else if($(event.target).is('#addStatePointNote')) {
                $(event.currentTarget).find('.formItem').css('display', 'block');
                $(event.currentTarget).find('#addStatePointNote').css('display', 'none');
                if(point.note) {
                    $(event.currentTarget).find('textarea').val(point.note);
                }
                return false;
            }
            else if($(event.target).is('#statePointNote') || $(event.target).is('#statePointNoteWrap')) {
                return false;
            }
            else if($(event.target).is('#saveStatePointNote')) {
                point.note = $(event.target).siblings('#statePointNote').val();
                if(point.note) {
                    this.saveProjectData({groupName: groupName, pointName: pointName, point: point});
                }
            }
            else if($(event.target).is('#deleteStatePointNote') && point.note) {
                point.note = null;
                this.saveProjectData({groupName: groupName, pointName: pointName, point: point});
            }

            else {
                var action = $(event.target).attr('data-id');
                if(action && point.status !== action) {
                    point.status = action;
                    if('date' in point) {
                        if(point.status === 'finished') {
                            point.date = Util.getDate();
                        }
                        else {
                            point.date = null;
                        }
                    }
                    this.saveProjectData({groupName: groupName, pointName: pointName, point: point});
                }
            }

            $('#toggleStatePointTool').removeClass('showTool');
            $currentTarget.removeClass('selectedRow');
            this.toggleStateLink = null;
        },

        //
        saveProjectData: function(data) {
            var _this = this,
                requestParams = {
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
                };

            projectsModel.get('currentModel').save(_.extend({projectDataPatch: data}, requestParams), {
                patch: true,
                wait: true,
                success: function(model, response, options) {
                    if(response.status === 'OK') {
                        _this.setProjects(response.data);
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                }
            });
        },

        //
        calcProjectState: function(item) {
            var projData = item.get('projectData'),
                projectStateList = {
                    'Проектирование': 'design',
                    'Производство': 'production',
                    'Отгрузка': 'shipping'
                };
            item.set({'firstInworkPoint': null}, {silent: true});

            for(var pState = 0, pStateLength = projData.length; pState < pStateLength; pState++) {
                for(var point = 1, pointLength = projData[pState].length; point < pointLength; point++) {
                    if(projData[pState][point].status === 'inwork') {
                        item.set({'firstInworkPoint': projData[pState][point].title}, {silent: true});
                        return projectStateList[projData[pState][0].groupName];
                    }
                }
            }
            return 'shipping';   //all point.status finished
        },

        //
        setProjectState: function(state, currentPassword) {
            var _this = this,
                formData = {},
                requestParams = {
                    projectState: state,
                    currentPassword: currentPassword,
                    state: projectsModel.get('currentState'),
                    year: projectsModel.get('currentYear'),
                    collectionUpdatedTime: localStorage.getItem('collectionUpdatedTime')
                };
            if(state !== 'deleted') {
                formData = this.getFormData('changeProjectState');
                if(!formData) {
                    return false;
                }
            }
            _.extend(formData, requestParams);

            projectsModel.get('currentModel').save(formData, {
                patch: true,
                wait: true,
                success: function(model, response, options) {
                    Dialog.destroyDialog();
                    if(response.status === 'OK') {
                        if(state !== projectsModel.get('currentState')) {
                            _this.deleteThumbView(projectsModel.get('currentModel'));
                        }
                        if(response.data.msg) {
                            Main.showNotice({msg: response.data.msg, type: 'INFORM'});
                        }
                        _this.setProjects(response.data);
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                }
            });
        },

        //
        deleteThumbView: function(model) {
            Projects.projectToolsView.detachToolsPanel();
            var index = _.findIndex(this.viewCollection, function(item) {
                return item.model === model;
            });
            this.viewCollection.splice(index, 1);
            model.trigger('closeView');
            this.collection.remove(model);
        },

        //
        showFileView: function() {
            Util.requestJSON('projects/getProjectSpec', {
                projectId: projectsModel.get('currentModel').get('id')
            })
                .done(function(response) {
                    if(response.status === 'OK') {
                        URL = URL || webkitURL;
                        var fileType = response.data.slice(response.data.indexOf(':') + 1, response.data.indexOf(';')),
                            file = new Blob([Projects.b64StringToArrayBuffer(response.data)], {type: fileType}),
                            fileURL = URL.createObjectURL(file),
                            iOS = /iPad|iPhone|iPod/i.test(navigator.userAgent);

                        if(!iOS) {
                            var $downloadSpecLink = Projects.projectsView.$('#downloadProjectSpec');
                            $downloadSpecLink.attr({
                                href: fileURL,
                                type: fileType,
                                download: projectsModel.get('currentModel').get('projectName') + '_Спецификация'
                            })
                                [0].click();
                            $downloadSpecLink.attr({href: '#'}).removeAttr('type download');
                        }
                        else {
                            window.open(fileURL, '_blank');
                        }
                        setTimeout(function() {
                            URL.revokeObjectURL(fileURL);
                        }, 200);
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                });
        }
    });


    //component API
    projectList.loadComponent = function() {
        projectsModel = Projects.projectsModel;
        props = Projects.props;
        Projects.projectListView = new ProjectListView();
    };


    return projectList;

})(window, jQuery, Backbone, _);
