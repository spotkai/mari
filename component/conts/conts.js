var Conts = (function(window, $, Backbone, _) {
    "use strict";

    var conts = {},     //instance
        UNICODE_MAXCHAR = '\uFFFF';

    var ContsView = Backbone.View.extend({
        el: '#contsHeader',
        events: {
            'click #addContact': 'showAddContactDialog',
            'input #contsSearch input': 'filterContacts'
        },

        initialize: function() {
            this.$('#contsSearch input').focus();
        },

        //
        showAddContactDialog: function(event) {
            conts.showAddContactDialog(
                $(event.target),
                $(templates['addContact']()),
                'Добавить контакт',
                function() {
                    conts.contactListView.addContact();
                });
        },

        //
        filterContacts: function(event) {
            var collection = conts.contactListView.collection,
                filterString = $(event.target).val().toUpperCase();
            event.preventDefault();
            conts.contactListView.resetCollection();

            if(filterString.length > 1) {
                var filteredCollection = collection.filter(function(model) {
                    var modelValues = _.values(model.attributes);
                    return _.some(modelValues, function(prop) {
                        return prop && !!~prop.toString().toUpperCase().indexOf(filterString);
                    });
                });
                conts.contactListView.filterString = filterString;
                conts.contactListView.resetCollection(filteredCollection);
            }
        },

        //
        clearSearchInput: function() {
            this.$('#contsSearch input').val('');
        }
    });


    var ContactModel = Backbone.Model.extend({
        defaults: {
            contactSurname: null,
            contactName: null,
            contactJob: null,
            contactCompany: null,
            contactTel: null,
            contactInet: null,
            contactAddress: null
        },

        initialize: function() {
            this.on('error', function(model, xhr, options) {
                Main.syncError(model, xhr, options);
            });
            this.on('invalid', function(model, error, options) {
                Dialog.showMsg(error);
            });
        },

        urlRoot: 'conts/rest',

        validate: function(attrs, options) {
            if(!attrs.contactSurname && !attrs.contactCompany) {
                return 'Укажите фамилию или компанию.';
            }

            if(!this.isNew()) {  //edit contact
                var modelChanged = false;
                for(var attr in attrs) {
                    if(attrs.hasOwnProperty(attr) && !_.isEqual(attrs[attr], this.get(attr))) {
                        modelChanged = true;
                        break;
                    }
                }
                if(!modelChanged) {
                    return 'Ничего не изменено.';
                }
            }
        }
    });


    //
    var ContactList = Backbone.Collection.extend({
        model: ContactModel,
        url: 'conts/rest',

        parse: function(response) {
            return response.data;
        },

        initialize: function() {
            this.columns = {
                contactSurname: "Фамилия",
                contactName: "Имя",
                contactJob: "Должность",
                contactCompany: "Компания",
                contactTel: "Телефон",
                contactInet: "Интернет",
                contactAddress: "Адрес"
            };
        }
    });


    //
    var ContactListView = Backbone.View.extend({
        el: '#contactList',
        events: {
            'click ._thead ._th.sortable': 'sortContacts',
            'click ._tbody ._tr': 'showCurrentContact',
            'click .showEditContact': 'showEditContact'
        },

        initialize: function() {
            this.sortAttr = 'contactCompany';
            this.sortOrder = 'ASC';
            this.collection = new ContactList();
            this.filterString = '';
            this.listenTo(this.collection, 'reset sort', this.render);
            this.listenTo(this.collection, 'error', function(collection, xhr, options) {
                Main.syncError(collection, xhr, options);
            });

            this.renderTableHeader();
            this.loadContacts();
        },

        //
        renderTableHeader: function() {
            this.$el.html(templates['contactListHeader']({columns: this.collection.columns}));
            this.$('._thead ._th[data-column=' + this.sortAttr + ']').addClass('activeHeader')
                .append('<span class="sortCaret ' + this.sortOrder + '"></span>');
            return this;
        },

        //
        loadContacts: function() {
            var _this = this;
            this.collection.fetch({
                reset: true,
                success: function(collection, response, options) {
                    conts.contsView.clearSearchInput();
                    if(response.status === 'OK') {
                        _this.originModels = response.data;
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                }
            });
        },

        //
        render: function() {
            this.currentContactId = null;
            this.sortModels();
            this.$('._tbody').html(templates['contactList']({collection: this.collection.models, filterString: this.filterString}));
            return this;
        },

        //
        sortModels: function() {
            var _this = this;
            this.collection.models = this.collection.sortBy(function(item) {
                var attr = item.get(_this.sortAttr);
                return attr ? attr.toUpperCase() : UNICODE_MAXCHAR;    //if no value move to end
            });
            if(this.sortOrder === 'DESC') {
                this.collection.models.reverse();
            }
        },

        //
        showCurrentContact: function(event) {
            var $contactRow = $(event.currentTarget);
            if(!$contactRow.hasClass('selectedContact')) {
                event.preventDefault();
                this.$('._tr.selectedContact').removeClass('selectedContact')
                    .find('.showEditContact').remove();
                $contactRow.addClass('selectedContact')
                    .prepend('<li class="showEditContact"></li>');
                this.currentContactId = $contactRow.attr('data-id');
            }
        },

        //
        sortContacts: function(event) {
            var $sortHeader = $(event.currentTarget);
            if(!$sortHeader.hasClass('activeHeader')) {
                this.sortOrder = 'ASC';
                this.$('._thead ._th.activeHeader').removeClass('activeHeader')
                    .find('.sortCaret').remove();
                $sortHeader.addClass('activeHeader')
                    .append('<span class="sortCaret ' + this.sortOrder + '"></span>');
            }
            else {
                if(this.sortOrder === 'ASC') {
                    this.sortOrder = 'DESC';
                    $sortHeader.find('.sortCaret').removeClass('ASC').addClass('DESC');
                }
                else if(this.sortOrder === 'DESC') {
                    this.sortOrder = 'ASC';
                    $sortHeader.find('.sortCaret').removeClass('DESC').addClass('ASC');
                }
            }
            this.sortAttr = $sortHeader.attr('data-column');
            this.render();
        },

        //
        addContact: function() {
            var _this = this,
                data = this.getFormData();

            (new ContactModel(data)).save({}, {
                success: function(model, response, options) {
                    if(response.status === 'OK') {
                        _this.loadContacts();
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                    Dialog.destroyDialog();
                }
            });
        },

        //
        showEditContact: function() {
            var currentModel = this.collection.get(this.currentContactId),
                $currentRow = this.$('._tbody ._tr[data-id=' + this.currentContactId + ']');

            conts.showAddContactDialog(
                $currentRow,
                $(templates['addContact'](currentModel.attributes)),
                (currentModel.get('contactSurname') || currentModel.get('contactCompany')) + ': Редактировать контакт',
                function() {
                    Conts.contactListView.editContact();
                });
        },

        //
        editContact: function() {
            var _this = this,
                data = this.getFormData(),
                currentModel = this.collection.get(this.currentContactId);

            currentModel.save(data, {
                patch: true,
                success: function(model, response, options) {
                    if(response.status === 'OK') {
                        _this.loadContacts();
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                    Dialog.destroyDialog();
                }
            });
        },

        //
        getFormData: function() {
            var data = {};
            data.contactTel = [];
            data.contactInet = [];

            $('#formAddContact').find('input, textarea').each(function() {
                if(~$(this).attr('name').indexOf('contactTel')) {
                    if($(this).val()) {
                        data.contactTel.push($(this).val());
                    }
                }
                else if(~$(this).attr('name').indexOf('contactInet')) {
                    if($(this).val()) {
                        var value = $(this).val().replace(/^https?:\/\//, '').replace(/\/$/, '');
                        data.contactInet.push(value);
                    }
                }
                else if($(this).is('input')) {
                    data[$(this).attr('name')] = $(this).val().replace(/"/g, '\'') || null;
                }
                else {
                    data[$(this).attr('name')] = $(this).val() || null;
                }
            });
            return data;
        },

        //
        deleteContact: function() {
            var _this = this,
                currentModel = this.collection.get(this.currentContactId);
            currentModel.destroy({
                wait: true,
                success: function(model, response, options) {
                    if(response.status === 'OK') {
                        Main.showNotice({msg: response.data, type: 'INFORM'});
                        _this.loadContacts();
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                    Dialog.destroyDialog();
                }
            });
        },

        //
        resetCollection: function(models) {
            if(!models) {
                this.filterString = '';
            }
            this.collection.reset(models || this.originModels);
        }
    });


    //component API
    conts.loadComponent = function() {
        this.contsView = new ContsView();
        this.contactListView = new ContactListView();
    };

    //
    conts.unloadComponent = function() {};

    //
    conts.showAddContactDialog = function($elem, $content, title, saveCB, beforeCloseCB) {
        var buttons = {
            'Сохранить': saveCB
        };
        if(~title.indexOf('Редактировать контакт')) {
            buttons['Удалить контакт'] = function() {
                conts.contactListView.deleteContact();
            };
        }
        Dialog.makeDialog({
            $elem: $elem,
            $content: $content,
            title: title,
            buttons: buttons,
            beforeClose: beforeCloseCB
        });
        var $contactText = $('#dialogContent').find('input[name=contactSurname]');
        if($.trim($contactText.val()) == '') {
            $contactText.focus();
        }
    };

    //
    conts.reloadPage = function() {
        this.contactListView.loadContacts();
    };

    //
    conts.getAjaxRequestDescription = function(settings) {
        if(~settings.url.indexOf('conts/rest')) {
            var typeList = {
                'GET': 'загрузка контактов',
                'POST': 'добавление контакта',
                'PATCH': 'обновление контакта',
                'DELETE': 'удаление контакта'
            };
            return typeList[settings.type];
        }
    };


    return conts;

})(window, jQuery, Backbone, _);
