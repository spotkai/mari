var UserPanel = (function(window, $, Backbone, _) {
    "use strict";

    var userPanel = {},  //instance
        userView;

    //
    var UserModel = Backbone.Model.extend({
        defaults: {
            userName: null,
            userAvatar: null,
            userTasks: null,
            currentUserTask: null
        },
        urlRoot: 'userPanel/rest',

        initialize: function() {
            this.on('error', function(model, xhr, options) {
                Main.syncError(model, xhr, options);
            });
        },

        parse: function(response) {
            return response.data;
        }
    });


    //
    var UserView = Backbone.View.extend({
        tagName: 'div',
        id: 'user',
        events: {
            'click #userName li>a': function(ev) {
                ev.preventDefault();
                var href = $(ev.currentTarget).attr('href'),
                    action = href.slice(href.indexOf(':') + 1);
                this[action + 'Action'](ev);
            },
            'click #helpInfo': 'showHelpInfo',
            'click #userTasksList': 'showUserTasks',
            'click #userTasksContent li.userTask': 'selectUserTask',
            'click span.showEditUserTask': 'changeUserTask'
        },

        initialize: function() {
            this.currentUserTaskIndex = null;
            this.updateState = null;
            this.model = new UserModel();
            this.loadUserData();
        },

        //
        loadUserData: function() {
            var _this = this;
            this.model.fetch({
                success: function(model, response, options) {
                    _this.render();
                    if(response.status === 'OK') {
                        if(_this.updateState === 'userData_updated') {
                            vent.trigger('userData:updated');
                        }
                        else if(_this.updateState === 'userData_tasksUpdated') {
                            _this.showUserTasks();
                        }
                        else {
                            vent.trigger('userData:loaded');
                        }
                        _this.updateState = null;
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                }
            });
        },

        //
        render: function() {
            this.$el.html(templates['userPanel'](this.model.attributes));
            this.$('#userTasksCount').html(this.model.get('userTasks').length);
            return this;
        },


        //
        addUserTaskAction: function(event) {
            var _this = this;
            this.showUserDialog($(event.target),
                $(templates['addUserTask']()),
                this.model.get('userName') + ': Добавить задачу',
                function() {
                    _this.addUserTask();
                }
            );
        },


        //
        addUserTask: function() {
            var userTask = $('#formAddTask').find('textarea').val() || null;
            if(userTask) {
                var tasks = this.model.get('userTasks');
                tasks.unshift(userTask);
                this.saveUserModelTasks(tasks);
            }
            else {
                Dialog.showMsg('Введите текст задачи');
            }
        },


        //
        changeUserTask: function(ev) {
            var _this = this,
                $userTaskElement = $(ev.target).closest('li'),
                selectedUserTaskId = $userTaskElement.attr('data-id'),
                selectedUserTaskIndex = selectedUserTaskId.slice(selectedUserTaskId.indexOf('_') + 1) || null,
                selectedUserTask = this.model.get('userTasks')[selectedUserTaskIndex];
            this.currentUserTaskIndex = selectedUserTaskIndex;
            this.showUserDialog($userTaskElement,
                $(templates['addUserTask']({taskText: selectedUserTask})),
                this.model.get('userName') + ': Редактировать задачу',
                function() {
                    _this.model.get('userTasks').splice(selectedUserTaskIndex, 1);
                    _this.addUserTask();
                });
        },


        //
        deleteUserTask: function() {
            var selectedUserTaskIndex = this.currentUserTaskIndex,
                tasks = this.model.get('userTasks');
            tasks.splice(selectedUserTaskIndex, 1);
            this.saveUserModelTasks(tasks);
        },


        //
        saveUserModelTasks: function(tasks) {
            var _this = this;
            this.model.save({userTasks: tasks}, {
                patch: true,
                success: function(model, response, options) {
                    Dialog.destroyDialog();
                    if(response.status === 'OK') {
                        _this.updateState = 'userData_tasksUpdated';
                        _this.loadUserData();
                    }
                    else if(response.status === 'ERROR') {
                        Main.showNotice({msg: response.data});
                    }
                }
            });
            this.currentUserTaskIndex = null;
        },


        //
        saveUserData: function() {
            var _this = this,
                formData = {
                    currentPassword: $('#currentPassword').val(),
                    inputPassword: $('#inputPassword').val(),
                    userAvatar: $('#userAvatar').siblings('span.loadedImg').css('background-image')
                        .replace('url(', '').replace(')', '').replace(/^"(.*)"$/, '$1')
                };
            if(formData['currentPassword']) {
                this.model.save(formData, {
                    success: function(model, response, options) {
                        if(response.status === 'OK') {
                            _this.updateState = 'userData_updated';
                            Dialog.destroyDialog();
                            Main.showNotice({msg: response.data, type: 'INFORM'});
                            _this.loadUserData();
                        }
                        else if(response.status === 'ERROR') {
                            Dialog.showMsg(response.data);
                        }
                    }
                });
            }
            else {
                Dialog.showMsg('Введите текущий пароль.');
            }
        },


        //
        logoutAction: function() {
            window.location.replace("main/logout");
        },


        //
        userSettingsAction: function(event) {
            var _this = this;
            this.showUserDialog($(event.target),
                $(templates['userData']({userData: this.model.attributes, action: 'userSettings'})),
                this.model.get('userName') + ': Редактировать профиль',
                function() {
                    _this.saveUserData();
                });
        },


        //
        deleteProjectAction: function(event) {
            if(Backbone.history.fragment !== '!projects' || !Projects.projectsModel.get('currentModel')) {
                return false;
            }

            var _this = this;
            Projects.showDeletingProject();
            this.showUserDialog($(event.target),
                $(templates['userData']({action: 'deleteProject'})),
                _this.model.get('userName') + ': Удаление проекта',
                function() {
                    var currentPassword = $('#userData').find('#currentPassword').val();
                    if(currentPassword) {
                        Projects.setProjectState('deleted', currentPassword);
                    }
                    else {
                        Dialog.showMsg('Введите текущий пароль.');
                    }
                },
                function() {
                    Projects.cancelDeletingProject();
                }
            );
        },


        //
        showUserDialog: function($elem, $content, title, saveCB, beforeCloseCB) {
            var buttons = {},
                buttonName = 'Сохранить';
            if(~title.indexOf('Удаление проекта')) {
                buttonName = 'Удалить';
            }
            else if(~title.indexOf('Редактировать профиль')) {
                buttonName = 'Изменить';
            }
            buttons[buttonName] = saveCB;

            if(~title.indexOf('Редактировать задачу')) {
                buttons['Удалить задачу'] = function() {
                    userView.deleteUserTask();
                }
            }
            Dialog.makeDialog({
                $elem: $elem,
                $content: $content,
                title: title,
                buttons: buttons,
                beforeClose: beforeCloseCB
            });
        },


        //
        showHelpInfo: function() {
            Main.showNotice({
                title: 'Подсказки',
                msg: templates['helpInfo'](),
                type: 'INFORM',
                keep: true,
                width: '330px'
            });
        },


        //
        showUserTasks: function() {
            this.model.set({'currentUserTask': null}, {silent: true});
            this.$('#userTasksList').toggleClass('active');
            if(this.$('#userTasksList').is('.active')) {
                this.$('#userTasksContent').addClass('scene')
                    .html(templates['userTaskList']({userTasks: this.model.get('userTasks')}));
            }
            else {
                this.$('#userTasksContent').removeClass('scene')
                    .html('');
            }
        },


        //
        selectUserTask: function(ev) {
            if(ev.target.tagName !== 'LI') {
                return false;
            }
            var $currentTask = $(ev.currentTarget).attr('data-id'),
                currentTaskId = $currentTask.slice($currentTask.indexOf('_') + 1) || null;
            this.model.set('currentUserTask', currentTaskId);

            this.$('li.userTask').removeClass('selectedTask')
                .find('.showEditUserTask').remove();
            $(ev.currentTarget).addClass('selectedTask').append('<span class="showEditUserTask hoverCircle"></span>');
        }

    });


    //component API
    userPanel.loadComponent = function() {
        userView = new UserView();
        return userView.el;
    };

    userPanel.getUserModelData = function(param) {
        return userView.model.get(param);
    };

    userPanel.getAjaxRequestDescription = function(settings) {
        if(~settings.url.indexOf('userPanel/rest')) {
            var typeList = {
                'GET': 'загрузка пользователя',
                'PUT': 'обновление пользователя',
                'PATCH': 'обновление задач пользователя'
            };
            return typeList[settings.type];
        }
    };


    return userPanel;

})(window, jQuery, Backbone, _);
