# z-layers #

* 00 - basic
* 08 - thumbLimiter
* 11 - tool-icons, pickadateContainer, projectDetails, showEditTask

* 21 - headers (fixed) / userTasksContent
* 22 - mainHeader (fixed) / dropdown menus / showEditUserTask

* 31 - shadow wrapper

* 41 - appMsg div / dialog div
