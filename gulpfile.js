//
var gulp = require('gulp'),
    del = require('del'),
    concat = require('gulp-concat'),
    clip = require('gulp-clip-empty-files'),
    csso = require('gulp-csso'),
    imagemin = require('gulp-imagemin'),
    less = require('gulp-less'),
    rename = require('gulp-rename'),
    templateCompile = require('gulp-template-compile'),
    uglify = require('gulp-uglify');

var BUILD_DIR = 'build/',
    RES_DIR = BUILD_DIR + 'resources',
    IMG_SRC = ['layout/*/*.{png,jpg,gif}', 'component/*/*.{png,jpg,gif}'],
    FONT_SRC = ['layout/*/*.{woff,otf}', 'component/*/*.{woff,otf}'],
    app = [  //layouts dependencies //order by dependency ['layoutName', dep1, dep2(dep1)]
        ['login', '_common', 'util', 'monitor'],
        ['error', '_common'],
        ['main', '_common', 'util', 'monitor', 'dropdown', 'accordion', 'userPanel', 'dialog', 'projects', 'docs', 'conts']
    ];

//build
gulp.task('build', function() {
    cleanTask();
    setTimeout(function() {
        buildTask();
    }, 330);
    setTimeout(function() {
        watchTask();
    }, 13330);
});

//build task
function buildTask() {
    app.forEach(function(layout) {
        processStyles(layout);
        processScripts(layout);
        processTemplates(layout);
    });
    processImg();
    processFont();
}

//clean build task
function cleanTask() {
    del([BUILD_DIR + '**/*']);
}

//watch task
function watchTask() {
    app.forEach(function(layout) {
        var srcCss = getSrc(layout, '.less'),
            srcJs = getSrc(layout, '.js'),
            srcTpl = getSrc(layout, '.template.html');

        gulp.watch(srcCss, function() {
            processStyles(layout);
        });
        gulp.watch(srcJs, function() {
            processScripts(layout);
        });
        gulp.watch(srcTpl, function() {
            processTemplates(layout);
        });
        gulp.watch(IMG_SRC, function() {
            processImg();
        });
        gulp.watch(FONT_SRC, function() {
            processFont();
        });
    });
}

//
function processStyles(layout) {
    var layoutName = layout[0] + '.less',
        srcCss = getSrc(layout, '.less'),
        dest = getDest(layout);
    gulp.src(srcCss)
        .pipe(concat(layoutName))
        .pipe(less())
        .pipe(csso())
        .pipe(rename({suffix: '.min', dirname: ''}))
        .pipe(clip())
        .pipe(gulp.dest(dest));
}

//
function processScripts(layout) {
    var layoutName = layout[0] + '.js',
        srcJs = getSrc(layout, '.js'),
        dest = getDest(layout);
    gulp.src(srcJs)
        .pipe(concat(layoutName))
        .pipe(uglify())
        .pipe(rename({suffix: '.min', dirname: ''}))
        .pipe(clip())
        .pipe(gulp.dest(dest));
}

//
function processTemplates(layout) {
    var layoutName = layout[0] + '.template.js',
        srcTpl = getSrc(layout, '.template.html'),
        dest = getDest(layout);
    gulp.src(srcTpl)
        .pipe(templateCompile({
            name: function(file) {
                return file.relative.slice(file.relative.lastIndexOf('\\') + 1, file.relative.indexOf('.template.html'));
            },
            namespace: 'templates'    //window['templates']['tplName']
        }))
        .pipe(concat(layoutName))
        .pipe(clip())
        .pipe(gulp.dest(dest));
}

//
function processImg() {
    gulp.src(IMG_SRC)
        .pipe(imagemin({optimizationLevel: 5, progressive: true, interlaced: true}))
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(RES_DIR));
}

//
function processFont() {
    gulp.src(FONT_SRC)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(RES_DIR));
}

//get src array
function getSrc(layout, ext) {    //ext with . (.js)
    var layoutName = layout[0],
        src = [];
    for(var i = 1, li = layout.length; i < li; i++) {
        var componentName = layout[i];
        src.push('component/' + componentName + '/*' + ext);
        src.push('component/' + componentName + '.*/*' + ext); //add sub-components
    }
    src.push('layout/' + layoutName + '/*' + ext);
    return src;
}

//get dest
function getDest(layout) {
    return BUILD_DIR + layout[0];
}